package com.ericsson.iec.mdus;

import static com.ericsson.iec.mdus.MdusWebservice.FIELD_ACTIVITY_NOTIFICATION_REQUEST;

import javax.jws.WebService;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import com.energyict.mdus.core.services.AsynchronousWebServiceParameters;

import wfmmdm01.interfaces.mdm.iec.FieldActivityNotificationRequest;
import wfmmdm01.interfaces.mdm.iec.FieldActivityNotificationResponse;
import wfmmdm01.interfaces.mdm.iec.MessageFault;
import wfmmdm01.interfaces.mdm.iec.ObjectFactory;

@WebService(endpointInterface = "wfmmdm01.interfaces.mdm.iec.WfmMdm01PortType")
public class WfmEndPoint extends IecEndPoint {

	public FieldActivityNotificationResponse setFieldActivityNotification(FieldActivityNotificationRequest request) throws MessageFault {
		String uuid = request.getMessageHeader().getMessageID();
		JAXBElement<FieldActivityNotificationRequest> element = new JAXBElement<FieldActivityNotificationRequest>(new QName("wfm", "FieldActivityNotificationRequest"), FieldActivityNotificationRequest.class, request);
		try {
			processForTomcat(new AsynchronousWebServiceParameters(FIELD_ACTIVITY_NOTIFICATION_REQUEST, element, uuid));
		} catch (Exception e) {
			return buildFieldActivityNotificationResponse("ERROR", e.toString());
		}
		return buildFieldActivityNotificationResponse("OK", "Also OK");
	 }

	private FieldActivityNotificationResponse buildFieldActivityNotificationResponse(String statusCode, String statusMessage) {
		ObjectFactory factory = new ObjectFactory();
		core.mdm.iec.ObjectFactory coreFactory = new core.mdm.iec.ObjectFactory(); 		

		FieldActivityNotificationResponse response = factory.createFieldActivityNotificationResponse();
		response.setMessageResponse(coreFactory.createMessageResponse());
		response.getMessageResponse().setStatusCode(statusCode);
		response.getMessageResponse().setStatusMessage(statusMessage);
		return response;
	}
}
