package com.ericsson.iec.mdus;

import static com.ericsson.iec.mdus.MdusWebservice.*;

import javax.jws.WebService;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import com.energyict.mdus.core.services.AsynchronousWebServiceParameters;

import sapmdm01.interfaces.mdm.iec.MessageFault;
import sapmdm01.interfaces.mdm.iec.ObjectFactory;
import sapmdm01.interfaces.mdm.iec.MeterSapLinkRequest;
import sapmdm01.interfaces.mdm.iec.MeterSapLinkResponse;
import sapmdm01.interfaces.mdm.iec.MeterLocationRequest;
import sapmdm01.interfaces.mdm.iec.MeterLocationResponse;
import sapmdm01.interfaces.mdm.iec.MeterPodRequest;
import sapmdm01.interfaces.mdm.iec.MeterPodResponse;

@WebService(endpointInterface = "sapmdm01.interfaces.mdm.iec.SapMdm01PortType")
public class SapEndPoint extends IecEndPoint {

	public MeterSapLinkResponse setMeterSapLink(MeterSapLinkRequest request) throws MessageFault {
		String uuid = request.getMessageHeader().getMessageID();
		JAXBElement<MeterSapLinkRequest> element = new JAXBElement<MeterSapLinkRequest>(new QName("ms", "MeterSapLinkRequest"), MeterSapLinkRequest.class, request);
		try {
			processForTomcat(new AsynchronousWebServiceParameters(METER_SAP_LINK_REQUEST, element, uuid));
		} catch (Exception e) {
			return buildMeterSapLinkResponse("1", e.toString());
		}
		return buildMeterSapLinkResponse("0", "Success");
	}
	
	private MeterSapLinkResponse buildMeterSapLinkResponse(String statusCode, String statusMessage) {
		ObjectFactory factory = new ObjectFactory();
		core.mdm.iec.ObjectFactory coreFactory = new core.mdm.iec.ObjectFactory(); 		

		MeterSapLinkResponse response = factory.createMeterSapLinkResponse();
		response.setMessageResponse(coreFactory.createMessageResponse());
		response.getMessageResponse().setStatusCode(statusCode);
		response.getMessageResponse().setStatusMessage(statusMessage);
		return response;
	}

	public MeterLocationResponse setMeterLocation(MeterLocationRequest request) throws MessageFault {
		String uuid = request.getMessageHeader().getMessageID();
		JAXBElement<MeterLocationRequest> element = new JAXBElement<MeterLocationRequest>(new QName("ml", "MeterLocationRequest"), MeterLocationRequest.class, request);
		try {
			processForTomcat(new AsynchronousWebServiceParameters(METER_LOCATION_REQUEST, element, uuid));
		} catch (Exception e) {
			return buildMeterLocationResponse("1", e.toString());
		}
		return buildMeterLocationResponse("0", "Success");
	}
	
	private MeterLocationResponse buildMeterLocationResponse(String statusCode, String statusMessage) {
		core.mdm.iec.ObjectFactory coreFactory = new core.mdm.iec.ObjectFactory(); 
		ObjectFactory factory = new ObjectFactory();

		MeterLocationResponse response = factory.createMeterLocationResponse();
		response.setMessageResponse(coreFactory.createMessageResponse());
		response.getMessageResponse().setStatusCode(statusCode);
		response.getMessageResponse().setStatusMessage(statusMessage);
		return response;
	}

	public MeterPodResponse setMeterPod(MeterPodRequest request) throws MessageFault {
		String uuid = request.getMessageHeader().getMessageID();
		JAXBElement<MeterPodRequest> element = new JAXBElement<MeterPodRequest>(new QName("mp", "MeterPodRequest"), MeterPodRequest.class, request);
		try {
			processForTomcat(new AsynchronousWebServiceParameters(METER_POD_REQUEST, element, uuid));
		} catch (Exception e) {
			return buildMeterPodResponse("1", e.toString());
		}
		return buildMeterPodResponse("0", "Success");
	}
	
	private MeterPodResponse buildMeterPodResponse(String statusCode, String statusMessage) {
		ObjectFactory factory = new ObjectFactory();
		core.mdm.iec.ObjectFactory coreFactory = new core.mdm.iec.ObjectFactory(); 
		
		MeterPodResponse response = factory.createMeterPodResponse();
		response.setMessageResponse(coreFactory.createMessageResponse());
		response.getMessageResponse().setStatusCode(statusCode);
		response.getMessageResponse().setStatusMessage(statusMessage);
		return response;
	}
}
