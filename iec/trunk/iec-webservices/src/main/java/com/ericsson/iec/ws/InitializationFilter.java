package com.ericsson.iec.ws;

import java.io.IOException;
import java.util.Properties;
import java.util.stream.Stream;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.energyict.cpo.Environment;
import com.energyict.mdw.core.MeteringWarehouseFactory;

public class InitializationFilter implements javax.servlet.Filter {

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		chain.doFilter(request, response);
		
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		Properties props = new Properties();
		Stream.of("jdbcUrl" , "dbUser" , "dbPassword").forEach( key -> props.put(key, config.getServletContext().getInitParameter(key)));
		Environment.setDefault(props);
		new MeteringWarehouseFactory().getBatch(true);
	}
	

	
}
