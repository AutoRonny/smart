package com.ericsson.iec.ws;

import java.io.StringWriter;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.energyict.cbo.BusinessException;
import com.energyict.cpo.Environment;
import com.energyict.mdw.core.MeteringWarehouse;
import com.energyict.mdw.core.MeteringWarehouseFactory;
import com.energyict.mdw.messaging.MessageService;

import nismdm01.interfaces.mdm.iec.MessageFault;
import nismdm01.interfaces.mdm.iec.NisMdm01PortType;
import nismdm01.interfaces.mdm.iec.ObjectFactory;
import nismdm01.interfaces.mdm.iec.PremiseRequest;
import nismdm01.interfaces.mdm.iec.PremiseResponse;
import nismdm01.interfaces.mdm.iec.TransformerRequest;
import nismdm01.interfaces.mdm.iec.TransformerResponse;
import nismdm01.interfaces.mdm.iec.TransformerStationRequest;
import nismdm01.interfaces.mdm.iec.TransformerStationResponse;

@WebService(targetNamespace = "http://iec.mdm.interfaces.NisMdm01", name = "NisMdm01PortType", endpointInterface="nismdm01.interfaces.mdm.iec.NisMdm01PortType")
public class NisService implements NisMdm01PortType {
	
	private MeteringWarehouse mw;
	private final ObjectFactory factory = new ObjectFactory();
	private final core.mdm.iec.ObjectFactory coreFactory = new core.mdm.iec.ObjectFactory(); 
	private static final String DB_POOL_NAME = "java:comp/env/jdbc/eiserver";
	

	public NisService() {
		
		//Environment.getDefault().setPoolName(DB_POOL_NAME);
		//mw = new MeteringWarehouseFactory().getBatch();
		//mw.getFolderTypeFactory().findAll().forEach(System.out::println);
	}
	
	public TransformerStationResponse setTransformerStation(TransformerStationRequest request) throws MessageFault {
		try {
			sendMessage(request, "TransformerStation");
		} catch (JAXBException | JMSException | BusinessException | SQLException e) {
			throw new MessageFault(e.toString(), null, e);
		}
		TransformerStationResponse response = factory.createTransformerStationResponse();
		response.setMessageResponse(coreFactory.createMessageResponse());
		response.getMessageResponse().setStatusCode("OK");
		response.getMessageResponse().setStatusMessage("Also OK");
		return response;
	}

	public PremiseResponse setPremise(PremiseRequest request) throws MessageFault {
		try {
			sendMessage(request, "Premise");
		} catch (JAXBException | JMSException | BusinessException | SQLException e) {
			throw new MessageFault(e.toString(), null, e);
		}
		PremiseResponse response = factory.createPremiseResponse();
		response.setMessageResponse(coreFactory.createMessageResponse());
		response.getMessageResponse().setStatusCode("OK");
		response.getMessageResponse().setStatusMessage("Also OK");
		return response;
	}

	public TransformerResponse setTransformer(TransformerRequest request) throws MessageFault {
		try {
			sendMessage(request,  "Transformer");
		} catch (JAXBException | JMSException | BusinessException | SQLException e) {
			throw new MessageFault(e.toString(), null, e);
		}
		TransformerResponse response = factory.createTransformerResponse();
		response.setMessageResponse(coreFactory.createMessageResponse());
		response.getMessageResponse().setStatusCode("OK");
		response.getMessageResponse().setStatusMessage("Also OK");
		return response;
	}
	
	private void sendMessage(Object xml, String messageServiceName) throws BusinessException, SQLException, JMSException, JAXBException {
		MessageService service = mw.getMessageServiceFactory().find(messageServiceName);
		TextMessage message = service.createTextMessage();
		message.setText(getPayload(xml));
		service.send(message, 0);
	}
	
	private String getPayload(Object xml) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(xml.getClass());
		Marshaller marshaller = jaxbContext.createMarshaller();
		StringWriter writer = new StringWriter();
		marshaller.marshal(xml, writer);
		return writer.toString();
	}
	
}
