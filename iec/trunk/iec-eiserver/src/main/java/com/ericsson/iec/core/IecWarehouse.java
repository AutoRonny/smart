package com.ericsson.iec.core;

import java.util.LinkedHashMap;
import java.util.Map;

import com.energyict.mdw.core.MeterPortfolioFactory;
import com.energyict.mdw.core.MeterPortfolioItemFactory;
import com.energyict.mdw.core.MeteringWarehouse;
import com.ericsson.iec.mdus.IecClassInjector;
import com.ericsson.iec.model.CellularGridFactory;
import com.ericsson.iec.model.CellularGridFactoryImpl;
import com.ericsson.iec.model.CellularMeterFactory;
import com.ericsson.iec.model.CellularMeterFactoryImpl;
import com.ericsson.iec.model.CityFactory;
import com.ericsson.iec.model.CityFactoryImpl;
import com.ericsson.iec.model.CtMeterGridFactory;
import com.ericsson.iec.model.CtMeterGridFactoryImpl;
import com.ericsson.iec.model.DataConcentratorDeploymentFactory;
import com.ericsson.iec.model.DataConcentratorDeploymentFactoryImpl;
import com.ericsson.iec.model.DataConcentratorDeviceFactory;
import com.ericsson.iec.model.DataConcentratorDeviceFactoryImpl;
import com.ericsson.iec.model.DataConcentratorFactory;
import com.ericsson.iec.model.DataConcentratorFactoryImpl;
import com.ericsson.iec.model.DistrictFactory;
import com.ericsson.iec.model.DistrictFactoryImpl;
import com.ericsson.iec.model.FirmwareVersionFactory;
import com.ericsson.iec.model.FirmwareVersionFactoryImpl;
import com.ericsson.iec.model.GenericMeterDeviceFactory;
import com.ericsson.iec.model.GenericMeterDeviceFactoryImpl;
import com.ericsson.iec.model.MeterFactory;
import com.ericsson.iec.model.MeterLocationFactory;
import com.ericsson.iec.model.MeterLocationFactoryImpl;
import com.ericsson.iec.model.PlcGridFactory;
import com.ericsson.iec.model.PlcGridFactoryImpl;
import com.ericsson.iec.model.PlcMeterFactory;
import com.ericsson.iec.model.PlcMeterFactoryImpl;
import com.ericsson.iec.model.PodAssignmentFactory;
import com.ericsson.iec.model.PodAssignmentFactoryImpl;
import com.ericsson.iec.model.PointOfDeliveryFactory;
import com.ericsson.iec.model.PointOfDeliveryFactoryImpl;
import com.ericsson.iec.model.PremiseFactory;
import com.ericsson.iec.model.PremiseFactoryImpl;
import com.ericsson.iec.model.RegionFactory;
import com.ericsson.iec.model.RegionFactoryImpl;
import com.ericsson.iec.model.TransformerFactory;
import com.ericsson.iec.model.TransformerFactoryImpl;
import com.ericsson.iec.model.TransformerStationFactory;
import com.ericsson.iec.model.TransformerStationFactoryImpl;
import com.ericsson.iec.model.WarehouseFactory;
import com.ericsson.iec.model.WarehouseFactoryImpl;
import com.ericsson.iec.model.workflow.CellularMeterRegistrationFactory;
import com.ericsson.iec.model.workflow.CellularMeterRegistrationFactoryImpl;
import com.ericsson.iec.model.workflow.MeterRegistrationNocNotificationFactory;
import com.ericsson.iec.model.workflow.MeterRegistrationNocNotificationFactoryImpl;
import com.ericsson.iec.model.workflow.PlcMeterRegistrationFactory;
import com.ericsson.iec.model.workflow.PlcMeterRegistrationFactoryImpl;
import com.ericsson.iec.prototype.DataConcentratorProtoTypeFactory;
import com.ericsson.iec.prototype.DataConcentratorProtoTypeFactoryImpl;
import com.ericsson.iec.prototype.MeterProtoTypeFactory;
import com.ericsson.iec.prototype.MeterProtoTypeFactoryImpl;
import com.ericsson.iec.prototype.PointOfDeliveryProtoTypeFactory;
import com.ericsson.iec.prototype.PointOfDeliveryProtoTypeFactoryImpl;

public class IecWarehouse {
	
	private static IecWarehouse INSTANCE;
	private CityFactory cityFactory;
	private DistrictFactory districtFactory;
	private RegionFactory regionFactory;
	private TransformerStationFactory transformerStationFactory;
	private TransformerFactory transformerFactory;
	private PremiseFactory premiseFactory;
	private CellularGridFactory cellularGridFactory;
	private PlcGridFactory plcGridFactory;
	private CtMeterGridFactory ctMeterGridFactory;
	private MeterLocationFactory meterLocationFactory;
	private PointOfDeliveryFactory pointOfDeliveryFactory;
	private PodAssignmentFactory podAssignmentFactory;
	private GenericMeterDeviceFactory genericMeterDeviceFactory;
	private MeterFactory meterFactory;
	private PlcMeterFactory plcMeterFactory;
	private CellularMeterFactory cellularMeterFactory;
	private DataConcentratorFactory dataConcentratorFactory;
	private DataConcentratorDeviceFactory dataConcentratorDeviceFactory;
	private IecClassInjector iecClassInjector;
	private MeteringWarehouse meteringWarehouse;
	private WarehouseFactory warehouseFactory;
	private FirmwareVersionFactory firmwareVersionFactory;
	private IecServiceProvider iecServiceProvider;
	private DataConcentratorProtoTypeFactory dataConcentratorProtoTypeFactory;
	private PointOfDeliveryProtoTypeFactory pointOfDeliveryProtoTypeFactory;
	private Map<String, MeterProtoTypeFactory> meterProtoTypeFactory;
	private MeterPortfolioFactory meterPortfolioFactory;
	private MeterPortfolioItemFactory meterPortfolioItemFactory;
	private DataConcentratorDeploymentFactory dataConcentratorDeploymentFactory;
	private PlcMeterRegistrationFactory plcMeterRegistrationFactory;
	private CellularMeterRegistrationFactory cellularMeterRegistrationFactory;
	private MeterRegistrationNocNotificationFactory meterRegistrationNocNotificationFactory;
	
	private IecWarehouse() {		
	}
	
	public CityFactory getCityFactory() {
		if (cityFactory == null) {
			cityFactory = new CityFactoryImpl();
		}
		return cityFactory;
	}
	
	public DistrictFactory getDistrictFactory() {
		if (districtFactory == null) {
			districtFactory = new DistrictFactoryImpl();
		}
		return districtFactory;
	}
	
	public RegionFactory getRegionFactory() {
		if (regionFactory == null) {
			regionFactory = new RegionFactoryImpl();
		}
		return regionFactory;
	}
	
	public TransformerStationFactory getTransformerStationFactory() {
		if (transformerStationFactory == null) {
			transformerStationFactory = new TransformerStationFactoryImpl();
		}
		return transformerStationFactory;
	}

	public TransformerFactory getTransformerFactory() {
		if (transformerFactory == null) {
			transformerFactory = new TransformerFactoryImpl();
		}
		return transformerFactory;
	}
	
	public PremiseFactory getPremiseFactory() {
		if (premiseFactory == null) {
			premiseFactory = new PremiseFactoryImpl();
		}
		return premiseFactory;
	}
	
	public CellularGridFactory getCellularGridFactory() {
		if (cellularGridFactory == null) {
			cellularGridFactory = new CellularGridFactoryImpl();
		}
		return cellularGridFactory;
	}
	
	public PlcGridFactory getPlcGridFactory() {
		if (plcGridFactory == null) {
			plcGridFactory = new PlcGridFactoryImpl();
		}
		return plcGridFactory;
	}
	
	public CtMeterGridFactory getCtMeterGridFactory() {
		if (ctMeterGridFactory == null) {
			ctMeterGridFactory = new CtMeterGridFactoryImpl();
		}
		return ctMeterGridFactory;
	}
	
	public MeterLocationFactory getMeterLocationFactory() {
		if (meterLocationFactory == null) {
			meterLocationFactory = new MeterLocationFactoryImpl();
		}
		return meterLocationFactory;
	}
	
	public PointOfDeliveryFactory getPointOfDeliveryFactory() {
		if (pointOfDeliveryFactory == null) {
			pointOfDeliveryFactory = new PointOfDeliveryFactoryImpl();
		}
		return pointOfDeliveryFactory;
	}

	public PodAssignmentFactory getPodAssignmentFactory() {
		if (podAssignmentFactory == null) {
			podAssignmentFactory = new PodAssignmentFactoryImpl();
		}
		return podAssignmentFactory;
	}

	public GenericMeterDeviceFactory getGenericMeterDeviceFactory() {
		if (genericMeterDeviceFactory == null) {
			genericMeterDeviceFactory = new GenericMeterDeviceFactoryImpl();
		}
		return genericMeterDeviceFactory;
	}
	
	public MeterFactory getMeterFactory() {
		if (meterFactory == null) {
			meterFactory = new MeterFactory();
		}
		return meterFactory;
	}
	
	public PlcMeterFactory getPlcMeterFactory() {
		if (plcMeterFactory == null) {
			plcMeterFactory = new PlcMeterFactoryImpl();
		}
		return plcMeterFactory;
	}
	
	public CellularMeterFactory getCellularMeterFactory() {
		if (cellularMeterFactory == null) {
			cellularMeterFactory = new CellularMeterFactoryImpl();
		}
		return cellularMeterFactory;
	}
	
	public DataConcentratorFactory getDataConcentratorFactory() {
		if (dataConcentratorFactory == null) {
			dataConcentratorFactory = new DataConcentratorFactoryImpl();
		}
		return dataConcentratorFactory;
	}
	
	public DataConcentratorDeviceFactory getDataConcentratorDeviceFactory() {
		if (dataConcentratorDeviceFactory == null) {
			dataConcentratorDeviceFactory = new DataConcentratorDeviceFactoryImpl();
		}
		return dataConcentratorDeviceFactory;
	}
	
	public static IecWarehouse getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new IecWarehouse();
		}
		return INSTANCE;
	}
	
	public IecClassInjector getIecClassInjector() {
		if (iecClassInjector == null) {
			iecClassInjector = new IecClassInjector();
		}
		return iecClassInjector;
	}
	
	public WarehouseFactory getWarehouseFactory() {
		if (warehouseFactory == null) {
			warehouseFactory = new WarehouseFactoryImpl();
		}
		return warehouseFactory;
	}
	

	public MeteringWarehouse getMeteringWarehouse() {
		if (meteringWarehouse == null) {
			meteringWarehouse = MeteringWarehouse.getCurrent();
		}
		return meteringWarehouse;
	}

	public void setMeteringWarehouse(MeteringWarehouse meteringWarehouse) {
		this.meteringWarehouse = meteringWarehouse;
	}

	public FirmwareVersionFactory getFirmwareVersionFactory() {
		if (firmwareVersionFactory == null) {
			firmwareVersionFactory = new FirmwareVersionFactoryImpl();
		}
		return firmwareVersionFactory;
	}

	public void setFirmwareVersionFactory(FirmwareVersionFactory firmwareVersionFactory) {
		this.firmwareVersionFactory = firmwareVersionFactory;
	}
	
	public IecServiceProvider getIecServiceProvider() {
		if (iecServiceProvider == null) {
			iecServiceProvider = new IecServiceProvider();
		}
		return iecServiceProvider;
		
	}

	public DataConcentratorProtoTypeFactory getDataConcentratorProtoTypeFactory() {
		if (dataConcentratorProtoTypeFactory == null) {
			dataConcentratorProtoTypeFactory = new DataConcentratorProtoTypeFactoryImpl();
		}
		return dataConcentratorProtoTypeFactory;
	}

	public void setDataConncentratorProtoTypeFactory(DataConcentratorProtoTypeFactory dataConncentratorProtoTypeFactory) {
		this.dataConcentratorProtoTypeFactory = dataConncentratorProtoTypeFactory;
	}

	public PointOfDeliveryProtoTypeFactory getPointOfDeliveryProtoTypeFactory() {
		if (pointOfDeliveryProtoTypeFactory == null) {
			pointOfDeliveryProtoTypeFactory = new PointOfDeliveryProtoTypeFactoryImpl();
		}
		return pointOfDeliveryProtoTypeFactory;
	}

	public void setPointOfDeliveryProtoTypeFactory(PointOfDeliveryProtoTypeFactory pointOfDeliveryProtoTypeFactory) {
		this.pointOfDeliveryProtoTypeFactory = pointOfDeliveryProtoTypeFactory;
	}

	public MeterProtoTypeFactory getMeterProtoTypeFactory(String meterModel) {
		if (meterProtoTypeFactory == null)
			meterProtoTypeFactory = new LinkedHashMap<String, MeterProtoTypeFactory>();
		MeterProtoTypeFactory mf = meterProtoTypeFactory.get(meterModel);
		if (mf == null) {
			mf = new MeterProtoTypeFactoryImpl(meterModel);
			meterProtoTypeFactory.put(meterModel, mf);
		}
		return mf;
	}

	public void setMeterProtoTypeFactory(MeterProtoTypeFactory meterProtoTypeFactory) {
		if (this.meterProtoTypeFactory == null)
			this.meterProtoTypeFactory = new LinkedHashMap<String, MeterProtoTypeFactory>();
		this.meterProtoTypeFactory.put(meterProtoTypeFactory.getMeterModel(), meterProtoTypeFactory);
	}

	public MeterPortfolioFactory getMeterPortfolioFactory() {
		if (meterPortfolioFactory == null) {
			meterPortfolioFactory = MeteringWarehouse.getCurrent().getMeterPortfolioFactory();
		}
		return meterPortfolioFactory;
	}

	public MeterPortfolioItemFactory getMeterPortfolioItemFactory() {
		if (meterPortfolioItemFactory == null) {
			meterPortfolioItemFactory = MeteringWarehouse.getCurrent().getMeterPortfolioItemFactory();
		}
		return meterPortfolioItemFactory;
	}

	public DataConcentratorDeploymentFactory getDataConcentratorDeploymentFactory() {
		if (dataConcentratorDeploymentFactory == null) {
			dataConcentratorDeploymentFactory = new DataConcentratorDeploymentFactoryImpl();
		}
		return dataConcentratorDeploymentFactory;
	}

	public void setDataConcentratorDeploymentFactory(DataConcentratorDeploymentFactory dataConcentratorDeploymentFactory) {
		this.dataConcentratorDeploymentFactory = dataConcentratorDeploymentFactory;
	}

	public PlcMeterRegistrationFactory getPlcMeterRegistrationFactory() {
		if (plcMeterRegistrationFactory == null) {
			plcMeterRegistrationFactory = new PlcMeterRegistrationFactoryImpl();
		}
		return plcMeterRegistrationFactory;
	}

	public CellularMeterRegistrationFactory getCellularMeterRegistrationFactory() {
		if (cellularMeterRegistrationFactory == null) {
			cellularMeterRegistrationFactory = new CellularMeterRegistrationFactoryImpl();
		}
		return cellularMeterRegistrationFactory;
	}
	
	public MeterRegistrationNocNotificationFactory getMeterRegistrationNocNotificationFactory() {
		if (meterRegistrationNocNotificationFactory == null) {
			meterRegistrationNocNotificationFactory = new MeterRegistrationNocNotificationFactoryImpl();
		}
		return meterRegistrationNocNotificationFactory;
	}
}
