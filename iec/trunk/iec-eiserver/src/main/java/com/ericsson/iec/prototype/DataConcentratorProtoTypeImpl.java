package com.ericsson.iec.prototype;

import static com.ericsson.iec.constants.ProtoTypeConstants.CircutorDCParameters.DATACONCENTRATOR_ID;
import static com.ericsson.iec.constants.ProtoTypeConstants.CircutorDCParameters.FIRMWARE_VERSION;
import static com.ericsson.iec.constants.ProtoTypeConstants.CircutorDCParameters.MAC_ADDRESS_PLC;
import static com.ericsson.iec.constants.ProtoTypeConstants.CircutorDCParameters.MANUFACTURER;
import static com.ericsson.iec.constants.ProtoTypeConstants.CircutorDCParameters.MANUFACTURING_YEAR;
import static com.ericsson.iec.constants.ProtoTypeConstants.CircutorDCParameters.MODEL_DESCRIPTION;
import static com.ericsson.iec.constants.ProtoTypeConstants.CircutorDCParameters.SERIAL_NUMBER;

import java.math.BigDecimal;

import com.energyict.mdw.core.Folder;
import com.energyict.mdw.template.CopySpec;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.DataConcentrator;

public class DataConcentratorProtoTypeImpl extends AbstractProtoTypeImpl<DataConcentrator> implements DataConcentratorProtoType {
	
	protected DataConcentratorProtoTypeImpl(CopySpec copySpec) {
		super(copySpec);
	}
	
	@Override
	public DataConcentrator wrapFolder(Folder folder) {
		return IecWarehouse.getInstance().getDataConcentratorFactory().createNewForLastVersion(folder);
	}
	
	@Override
	public void setDataConcentratorId(String id) {
		setStringParameter(DATACONCENTRATOR_ID.getName(), id);
	}

	@Override
	public void setManufacturer(String manufacturer) {
		setStringParameter(MANUFACTURER.getName(), manufacturer);
	}

	@Override
	public void setManufacturingYear(BigDecimal manufacturingYear) {
		setBigDecimalParameter(MANUFACTURING_YEAR.getName(), manufacturingYear);
	}

	@Override
	public void setModelDescription(String modelDescription) {
		setStringParameter(MODEL_DESCRIPTION.getName(), modelDescription);
	}

	@Override
	public void setMacAddressPLC(String macAddressPlc) {
		setStringParameter(MAC_ADDRESS_PLC.getName(), macAddressPlc);
	}

	@Override
	public void setFirmwareVersion(String firmwareVersion) {
		setStringParameter(FIRMWARE_VERSION.getName(), firmwareVersion);
	}

	@Override
	public void setSerialNumber(String serialNumber) {
		setStringParameter(SERIAL_NUMBER.getName(), serialNumber);
	}

}
