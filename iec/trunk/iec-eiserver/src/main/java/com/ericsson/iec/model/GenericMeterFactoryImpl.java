package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;

public abstract class GenericMeterFactoryImpl extends FolderVersionWrapperFactoryImpl<GenericMeter> implements GenericMeterFactory {

	public GenericMeterFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	public GenericMeterFactoryImpl() {
		super();
	}
}
