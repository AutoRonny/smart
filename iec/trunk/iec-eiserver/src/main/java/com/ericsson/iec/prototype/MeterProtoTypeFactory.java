package com.ericsson.iec.prototype;

public interface MeterProtoTypeFactory extends ProtoTypeFactory<MeterProtoType> {

	String getMeterModel();
}
