package com.ericsson.iec.core;

import java.util.List;

import com.energyict.mdw.core.SystemParameter;

public enum IecSystemParameters {

	NIS_MAX_NUMBER_OF_TRANSFORMERS("NIS_MAX_NUMBER_OF_TRANSFORMERS","Nis Max Number Of Transformers", "4 or 8", "Iec Config", "8", null);
	
	private String key;
	private String displayName;
	private String description;
	private String category;
	private String value;
	private List<String> values;
	
	IecSystemParameters(String key, String displayName, String description, String category, String value, List<String> values) {
		this.key = key;
		this.displayName = displayName;
		this.description = description;
		this.category = category;
		this.value = value;
		this.values = values;	
	}
	
	public IecSystemParameterSpec toParameterSpec() {
		return new IecSystemParameterSpec(key, displayName, description, category, value, values);
	}
	
	public String getStringValue() {
		SystemParameter param = IecWarehouse.getInstance().getMeteringWarehouse().getSystemParameterFactory().find(key);
		return param.getValue();
	}
	
	public int getIntValue() {
		SystemParameter param = IecWarehouse.getInstance().getMeteringWarehouse().getSystemParameterFactory().find(key);
		return Integer.valueOf(param.getValue());
	}
}




