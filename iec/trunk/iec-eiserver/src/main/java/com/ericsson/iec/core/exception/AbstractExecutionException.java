package com.ericsson.iec.core.exception;

import com.energyict.mdus.core.BusinessErrorCodeDelegate;
import com.energyict.mdus.core.BusinessErrorSeverity;
import com.energyict.mdus.core.MdusModuleHelper;
import com.energyict.mdus.core.SapProcessingResultCode;
import com.ericsson.iec.core.IecMdusCategoryCodes;
import com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage;

public abstract class AbstractExecutionException extends IecException {

	private static final long serialVersionUID = 8774856218981257439L;

	public AbstractExecutionException(ExecutionExceptionMessage exceptionCode, boolean fatal, Object... messageParameters) {
		super(exceptionCode, messageParameters);
		setFatal(fatal);
		setBusinessErrorCode(new BusinessErrorCodeDelegate(
								SapProcessingResultCode.FAILED, 
								BusinessErrorSeverity.ERROR,
								getMessage(),
								MdusModuleHelper.INSTANCE.get().getDefaultSapTypeId(),
								String.valueOf(IecMdusCategoryCodes.EXECUTION_FAILURE)
				));
	}

	public AbstractExecutionException(Throwable cause, ExecutionExceptionMessage exceptionCode, boolean fatal, Object... messageParameters) {
		super(cause, exceptionCode, messageParameters);
		setFatal(fatal);
		setBusinessErrorCode(new BusinessErrorCodeDelegate(
								SapProcessingResultCode.FAILED, 
								BusinessErrorSeverity.ERROR,
								getMessage(),
								MdusModuleHelper.INSTANCE.get().getDefaultSapTypeId(),
								String.valueOf(IecMdusCategoryCodes.EXECUTION_FAILURE)
				));
	}

}
