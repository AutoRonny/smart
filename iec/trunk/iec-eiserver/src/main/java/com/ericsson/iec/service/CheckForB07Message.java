package com.ericsson.iec.service;

import java.util.logging.Logger;

import com.energyict.mdc.messages.DeviceMessage;
import com.energyict.mdc.messages.DeviceMessageFactoryImpl;
import com.energyict.mdc.messages.DeviceMessageStatus;
import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.DeviceMessageFilter;
import com.ericsson.iec.model.DataConcentratorDeployment;

public class CheckForB07Message {
	
	public static final String TRANSITION_NAME = "Check For B07";
	
	private Logger logger;
	
	public CheckForB07Message(Logger logger) {
		this.logger = logger;
	}

	public final boolean process(DataConcentratorDeployment dataConcentratorDeployment) {
		DeviceMessageFilter filter = new DeviceMessageFilter();
		filter.setTrackingIdMask(dataConcentratorDeployment.getTrackingId());
		Device device = dataConcentratorDeployment.getDataConcentrator().getDevice().getDevice();
		for (DeviceMessage deviceMessage : new DeviceMessageFactoryImpl().findByDeviceAndFilter(device, filter)) {
			return deviceMessage.getStatus() == DeviceMessageStatus.FAILED 
					|| deviceMessage.getStatus() == DeviceMessageStatus.CONFIRMED;
		}
		return false;
	}

}
