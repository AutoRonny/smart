package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface RegionFactory extends FolderVersionWrapperFactory<Region> {

	Region findByKey(String key);
	
}
