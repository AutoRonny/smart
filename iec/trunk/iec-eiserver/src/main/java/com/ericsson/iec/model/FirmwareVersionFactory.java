package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface FirmwareVersionFactory extends FolderVersionWrapperFactory<FirmwareVersion> {

	FirmwareVersion findByKey(String key);
	
}
