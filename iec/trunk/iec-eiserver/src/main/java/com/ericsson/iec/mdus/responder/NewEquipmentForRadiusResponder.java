package com.ericsson.iec.mdus.responder;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.ResponseMessage;
import com.energyict.mdus.core.exception.MarshallingException;
import com.ericsson.iec.mdus.MdusSapEndpoint;
import iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.NewEqForRadiusRequest;
import iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.ObjectFactory;
import iec.tibco.resources.wsdl.concrete.ws_mdm_rad_01_services.WSMDMRAD01PortType;
import iec.tibco.resources.wsdl.concrete.ws_mdm_rad_01_services.WSMDMRAD01ServicesServiceagent;

public class NewEquipmentForRadiusResponder extends IecMdusSapResponder<WSMDMRAD01ServicesServiceagent, WSMDMRAD01PortType> {

	private ObjectFactory objectFactory;

	public NewEquipmentForRadiusResponder() throws MarshallingException {
		super(MdusSapEndpoint.NEW_EQUIPMENT_FOR_RADIUS, WSMDMRAD01ServicesServiceagent.class, WSMDMRAD01PortType.class);
	}

	@Override
	public void respond(ResponseMessage message) throws BusinessException {
		NewEqForRadiusRequest newEqRadiusRequest = unmarshal(message.getMessage(), NewEqForRadiusRequest.class);

		WSMDMRAD01PortType port = getPort();
		updateOutputRequest(message);
		try {
			port.newEqForRadiusOperation(newEqRadiusRequest); //.dcFaultNotificationOperation(dCFaultNotificationRequest);			
		}
		catch (iec.tibco.resources.wsdl.concrete.ws_mdm_rad_01_services.BasicFaultMessage e) {
			throw new BusinessException(e);
		}

	}

}
