package com.ericsson.iec.model;

import com.energyict.mdw.core.Folder;

public class WarehouseImpl implements Warehouse {

	private Folder folder;
	
	public WarehouseImpl(Folder folder) {
		this.folder = folder;
	}
	
	public Folder getFolder() {
		return folder;
	}
	
	
}
