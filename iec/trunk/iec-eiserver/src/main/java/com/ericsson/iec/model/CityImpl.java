package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;
import com.ericsson.iec.constants.AttributeTypeConstants.CityAttributes;
import com.ericsson.iec.core.IecWarehouse;

public class CityImpl extends FolderVersionWrapperImpl implements City {

	private static final long serialVersionUID = -1871659369835667822L;

	protected CityImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public District getDistrict() {
		return getFolderVersionWrapperAttribute(CityAttributes.DISTRICT.name, IecWarehouse.getInstance().getDistrictFactory());
	}
	@Override
	public void setDistrict(District district) {
		setFolderVersionWrapperAttribute(CityAttributes.DISTRICT.name, district);
	}

	@Override
	public Region getRegion() {
		return getFolderVersionWrapperAttribute(CityAttributes.REGION.name, IecWarehouse.getInstance().getRegionFactory());
	}
	@Override
	public void setRegion(Region region) {
		setFolderVersionWrapperAttribute(CityAttributes.REGION.name, region);
	}

	@Override
	public Date getLatestNisUpdateDate() {
		return getDateAndTimeAttribute(CityAttributes.LATEST_NIS_UPDATE_DATE.name);
	}
	@Override
	public void setLatestNisUpdateDate(Date latestNisUpdateDate) {
		setDateAndTimeAttribute(CityAttributes.LATEST_NIS_UPDATE_DATE.name, latestNisUpdateDate);
	}
}
