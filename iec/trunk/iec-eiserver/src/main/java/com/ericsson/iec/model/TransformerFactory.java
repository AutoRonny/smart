package com.ericsson.iec.model;

import java.util.List;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface TransformerFactory extends FolderVersionWrapperFactory<Transformer> {

	Transformer findByKey(String key);

	List<Transformer> findByTransformerStation(TransformerStation transformerStation);
	
}
