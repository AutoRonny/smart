package com.ericsson.iec.pluggable.workflow.meterregistration;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.energyict.annotations.processcaseadapter.WorkFlowService;
import com.energyict.cpo.PropertySpec;
import com.energyict.cpo.PropertySpecFactory;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractWorkflowServiceWithExceptionHandling;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.workflow.CellularMeterRegistration;
import com.ericsson.iec.model.workflow.CellularMeterRegistrationFactory;

@WorkFlowService(version = "1.0")
public class CellularMeterRegistrationWorkflowService extends AbstractWorkflowServiceWithExceptionHandling<CellularMeterRegistration>  {

	private static final String PROPERTY_USER_FILE_ID = "User File ID";
	
	@Override
	public String getVersion() {
		return "Version 1.0";
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<PropertySpec> getOptionalProperties() {
		return Collections.emptyList();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<PropertySpec> getRequiredProperties() {
		return Arrays.asList(PropertySpecFactory.bigDecimalPropertySpec(PROPERTY_USER_FILE_ID));
	}

	@Override
	public CellularMeterRegistrationFactory getProcessCaseWrapperFactory() {
		return IecWarehouse.getInstance().getCellularMeterRegistrationFactory();
	}

}
