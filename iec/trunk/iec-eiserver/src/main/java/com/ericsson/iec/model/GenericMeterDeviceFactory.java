package com.ericsson.iec.model;

import com.energyict.projects.common.model.device.DeviceWrapperFactory;

public interface GenericMeterDeviceFactory extends DeviceWrapperFactory<GenericMeterDevice> {

}
