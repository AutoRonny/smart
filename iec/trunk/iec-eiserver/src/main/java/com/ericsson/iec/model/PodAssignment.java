package com.ericsson.iec.model;

import com.energyict.projects.common.model.relation.RelationWrapper;

public interface PodAssignment extends RelationWrapper {

	PointOfDelivery getPointOfDelivery();
	void setPointOfDelivery(PointOfDelivery pointOfDelivery);

	GenericMeter getMeter();
	void setMeter(GenericMeter meter);

	String getUniqueIdentifier();
	void setUniqueIdentifier(String uniqueIdentifier);
}
