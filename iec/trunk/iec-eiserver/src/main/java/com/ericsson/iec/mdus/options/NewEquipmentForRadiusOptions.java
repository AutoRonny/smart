package com.ericsson.iec.mdus.options;

import java.util.Date;

public class NewEquipmentForRadiusOptions {

	private String serialId;
	private Date startDate;
	private Date endDate;

	public NewEquipmentForRadiusOptions(String serialId, Date startDate, Date endDate) {
		super();
		this.serialId = serialId;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public NewEquipmentForRadiusOptions(String requestMessage) {
		String[] splitStuff = requestMessage.split(";");
		this.serialId = splitStuff[0];
		this.startDate = new Date(Long.parseLong(splitStuff[1]));
		this.endDate = new Date(Long.parseLong(splitStuff[2]));
	}

	public String getSerialId() {
		return serialId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	@Override
	public String toString() {
		return serialId + ";" + startDate.getTime() + ";" + endDate.getTime();
	}

}