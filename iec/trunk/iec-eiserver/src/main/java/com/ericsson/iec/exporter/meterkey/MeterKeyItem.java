package com.ericsson.iec.exporter.meterkey;

import java.util.List;

public class MeterKeyItem {
    private String SerialNumberConcentrator;
    private List<MeterKeyModel> meterKeyModels;

    public MeterKeyItem(String serialNumberConcentrator, List<MeterKeyModel> meterKeyModels) {
        SerialNumberConcentrator = serialNumberConcentrator;
        this.meterKeyModels = meterKeyModels;
    }

    public String getSerialNumberConcentrator() {
        return SerialNumberConcentrator;
    }

    public List<MeterKeyModel> getMeterKeyModels() {
        return meterKeyModels;
    }

}
