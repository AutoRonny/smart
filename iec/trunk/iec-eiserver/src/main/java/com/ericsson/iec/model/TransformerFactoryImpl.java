package com.ericsson.iec.model;

import static com.energyict.projects.coreextensions.FolderTypeSearchFilter.CriteriumOperator.EQUALS;
import static com.ericsson.iec.constants.AttributeTypeConstants.TransformerAttributes.TRANSFORMER_STATION;
import static com.ericsson.iec.constants.FolderTypeConstants.FolderTypes.TRANSFORMER;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.energyict.mdw.core.Folder;
import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import com.energyict.projects.coreextensions.FolderTypeSearchFilter;
import com.ericsson.iec.core.IecWarehouse;

public class TransformerFactoryImpl extends FolderVersionWrapperFactoryImpl<Transformer> implements TransformerFactory {

	public TransformerFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	public TransformerFactoryImpl() {
		super();
	}

	@Override
	public Transformer createNew(FolderVersionAdapter arg0) {
		return new TransformerImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return TRANSFORMER.name;
	}
	
	@Override
	public Transformer findByKey(String key) {
		return findByExternalName(TRANSFORMER.buildExternalName(key), new Date());
	}
	
	@Override
	public List<Transformer> findByTransformerStation(TransformerStation transformerStation) {
		FolderTypeSearchFilter filter = new FolderTypeSearchFilter(getFolderType(), IecWarehouse.getInstance().getMeteringWarehouse().getRootFolder());
		filter.addCriterium(TRANSFORMER_STATION.getName(), EQUALS, transformerStation.getFolder());
		List<Transformer> transformers = new ArrayList<Transformer>();
		for (Folder folder : filter.findMatchingFolders()) {
			transformers.add(super.createNewForLastVersion(folder));
		}
		return transformers;
	}
}
