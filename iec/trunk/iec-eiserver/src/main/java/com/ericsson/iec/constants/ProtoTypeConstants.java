package com.ericsson.iec.constants;

public class ProtoTypeConstants {

	public enum ProtoTypes {
		CIRCUTOR_DATACONCENTRATOR("Circutor DC", ProtoTypeObjectType.DC),
		POINT_OF_DELIVERY("Point Of Delivery", ProtoTypeObjectType.POD),
		METER_061("061 - KAIFA PLC Single Phase Direct Meter", ProtoTypeObjectType.PLC_METER),
		METER_062("062 - KAIFA Cellular Single Phase Direct Meter", ProtoTypeObjectType.CELLULAR_METER),
		METER_510("510 - KAIFA Cellular Poly Phase Direct Meter", ProtoTypeObjectType.CELLULAR_METER),
		METER_741("741 - KAIFA Cellular Poly Phase CT Meter", ProtoTypeObjectType.CELLULAR_METER),
		METER_511("511 - ZIV PLC Poly Phase Direct Meter With Breaker", ProtoTypeObjectType.PLC_METER),
		METER_512("512 - ZIV PLC Poly Phase Direct Meter Without Breaker", ProtoTypeObjectType.PLC_METER),
		METER_742("742 - ZIV PLC Poly Phase CT Meter", ProtoTypeObjectType.PLC_METER),
		;
		
		private String protoTypeName; 			// Must be start with the correct code (Prototype name under "00 - Prototypes")
		private ProtoTypeObjectType objectType;	// Device Warehouse the meter should be placed in (By the meter shipment importer logic: PLC / Cellular)
		
		ProtoTypes(String protoTypeName, ProtoTypeObjectType objectType) {
			this.protoTypeName = protoTypeName;
			this.objectType = objectType;
		}
		
		public String getName() {
			return protoTypeName;
		}
		
		public ProtoTypeObjectType getObjectType() {
			return objectType;
		}
		
		static public ProtoTypes getByNamePrefix(String namePrefix) {
			for (ProtoTypes p : ProtoTypes.values()) {
				if (p.getName().startsWith(namePrefix))
					return p;
			}
			return null;
		}
	}
	
	public enum ProtoTypeObjectType {
		PLC_METER,
		CELLULAR_METER,
		CT_METER,
		DC,
		POD,
		;
	}
	
	public enum CircutorDCParameters {
		DATACONCENTRATOR_ID("DC_ID"),
		SERIAL_NUMBER("Serial Number"),
		MANUFACTURER("Manufacturer"),
		MANUFACTURING_YEAR("Manufacturing Year"),
		MODEL_DESCRIPTION("Model description"),
		MAC_ADDRESS_PLC("MAC Address PLC"),
		FIRMWARE_VERSION("Firmware Version"),
		;
		
		private String parameterName;
		
		CircutorDCParameters(String parameterName) {
			this.parameterName = parameterName;
		}
		
		public String getName() {
			return parameterName;
		}
	}

	public enum PointOfDeliveryParameters {
		POD_ID("podId"),
		;
		
		private String parameterName;
		
		PointOfDeliveryParameters(String parameterName) {
			this.parameterName = parameterName;
		}
		
		public String getName() {
			return parameterName;
		}
	}

	public enum MeterParameters {
		SERIAL_ID("Serial ID"),
		BATCH("Batch"),
		MAC_ADDRESS("MacAddress"),
		DELIVERY_DATE("Delivery Date"),
		FIRMWARE_VERSION("Firmware Version"),
		;
		
		private String parameterName;
		
		MeterParameters(String parameterName) {
			this.parameterName = parameterName;
		}
		
		public String getName() {
			return parameterName;
		}
	}
}
