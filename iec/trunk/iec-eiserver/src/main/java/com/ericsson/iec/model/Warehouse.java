package com.ericsson.iec.model;

import com.energyict.mdw.core.Folder;

public interface Warehouse {

	Folder getFolder();

}
