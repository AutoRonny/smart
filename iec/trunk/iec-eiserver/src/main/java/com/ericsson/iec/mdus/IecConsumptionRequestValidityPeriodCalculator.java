package com.ericsson.iec.mdus;

import com.energyict.cbo.TimePeriod;
import com.energyict.mdus.core.ConsumptionRequestValidityPeriodCalculator;

import nismdm01.interfaces.mdm.iec.PremiseRequest;
import nismdm01.interfaces.mdm.iec.TransformerRequest;
import nismdm01.interfaces.mdm.iec.TransformerStationRequest;
import nocmdm01.interfaces.mdm.iec.DCAssetRequest;
import nocmdm01.interfaces.mdm.iec.DCEventReportRequest;
import sapmdm01.interfaces.mdm.iec.MeterLocationRequest;
import sapmdm01.interfaces.mdm.iec.MeterPodRequest;
import sapmdm01.interfaces.mdm.iec.MeterSapLinkRequest;
import wfmmdm01.interfaces.mdm.iec.FieldActivityNotificationRequest;

public interface IecConsumptionRequestValidityPeriodCalculator extends ConsumptionRequestValidityPeriodCalculator {
	
	TimePeriod calculateForTransformerStationRequest(TransformerStationRequest request);
	TimePeriod calculateForTransformerRequest(TransformerRequest request);
	TimePeriod calculateForPremiseRequest(PremiseRequest request);
	TimePeriod calculateForDcAssetRequest(DCAssetRequest request);
	TimePeriod calculateForDcEventReportRequest(DCEventReportRequest request);
	TimePeriod calculateForFieldActivityNotificationRequest(FieldActivityNotificationRequest request);
	TimePeriod calculateForMeterSapLinkRequest(MeterSapLinkRequest request);
	TimePeriod calculateForMeterLocationRequest(MeterLocationRequest request);
	TimePeriod calculateForMeterPodRequest(MeterPodRequest request);
}

