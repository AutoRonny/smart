package com.ericsson.iec.mdus.handler.consumptionrequest;

import com.energyict.mdus.core.consumptionrequesthandler.ConsumptionRequestProcessingStatusListener;
import com.energyict.mdw.imp.ConsumptionRequest;
import com.energyict.projects.common.exceptions.CustomerException;

/**
 * Possible statuses resulting from the processing of a {@link ConsumptionRequest}.
 */
public enum ProcessingStatus
{
	SUCCESS {
		@Override
		void notify(ConsumptionRequestProcessingStatusListener listener, ConsumptionRequest consumptionRequest) throws CustomerException
		{
			listener.onSuccess(consumptionRequest);
		}
	},
	FATAL_ERROR {
		@Override
		void notify(ConsumptionRequestProcessingStatusListener listener, ConsumptionRequest consumptionRequest) throws CustomerException
		{
			listener.onFatalError(consumptionRequest);
		}
	},
	NON_FATAL_ERROR {
		@Override
		void notify(ConsumptionRequestProcessingStatusListener listener, ConsumptionRequest consumptionRequest) throws CustomerException
		{
			listener.onNonFatalError(consumptionRequest);
		}
	},
	SKIP {
		@Override
		void notify(ConsumptionRequestProcessingStatusListener listener, ConsumptionRequest consumptionRequest) throws CustomerException
		{
			listener.onSkip(consumptionRequest);
		}
	};

	/**
	 * Notifies the listener about the status resulting of processing of the {@link ConsumptionRequest}.
	 *
	 * @param listener to be notified.
	 * @param consumptionRequest processed.
	 * @throws CustomerException if any problem occurs during notification processing.
	 */
	abstract void notify(ConsumptionRequestProcessingStatusListener listener, ConsumptionRequest consumptionRequest) throws CustomerException;
}
