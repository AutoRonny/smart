package com.ericsson.iec.service;

import java.util.logging.Logger;

import com.energyict.mdc.messages.DeviceMessage;
import com.energyict.mdc.messages.DeviceMessageFactoryImpl;
import com.energyict.mdc.messages.DeviceMessageStatus;
import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.DeviceMessageFilter;
import com.energyict.mdw.service.ServiceRequest;
import com.energyict.mdw.service.ServiceRequestState;
import com.ericsson.iec.model.DataConcentratorDeployment;
import com.ericsson.iec.model.workflow.MeterRegistrationNocNotification;

public class CheckRegisteredNotificationRequest {
	
	public static final String TRANSITION_NAME = "Check Registered Notification";
	
	private Logger logger;
	
	public CheckRegisteredNotificationRequest(Logger logger) {
		this.logger = logger;
	}

	public final boolean process(MeterRegistrationNocNotification meterRegistrationNocNotification) {
		ServiceRequest serviceRequest = meterRegistrationNocNotification.getServiceRequest();
		return serviceRequest.getState() == ServiceRequestState.SUCCESS;
	}

}
