package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.coordinates.SpatialCoordinates;
import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface Premise extends FolderVersionWrapper {

	TransformerStation getTransformerStation();
	void setTransformerStation(TransformerStation transformerStation);

	Transformer getTransformer();
	void setTransformer(Transformer transformer);

	String getHighVoltageLineFeeder();
	void setHighVoltageLineFeeder(String feeder);

	String getNameHighVoltageLineFeeder();
	void setNameHighVoltageLineFeeder(String feederName);

	String getLowVoltageFeederId();
	void setLowVoltageFeederId(String feederId);

	String getPremiseType();
	void setPremiseType(String premiseType);

	String getMeterUsageType();
	void setMeterUsageType(String meterUsage);

	String getElectricNetType();
	void setElectricNetType(String electricNetType);

	String getDistrict();
	void setDistrict(String district);

	String getRegion();
	void setRegion(String region);

	String getCityCode();
	void setCityCode(String cityCode);

	String getCityName();
	void setCityName(String cityName);

	String getStreet();
	void setStreet(String street);

	String getStreetNumber();
	void setStreetNumber(String streetNumber);

	String getEntrance();
	void setEntrance(String entrance);

	String getFloor();
	void setFloor(String floor);

	String getApartment();
	void setApartment(String apartment);

	String getAddress();
	void setAddress(String address);

	SpatialCoordinates getCoordinates();
	void setCoordinates(SpatialCoordinates coordinates);

	Date getLatestNisUpdateDate();
	void setLatestNisUpdateDate(Date latestNisUpdateDate);
}
