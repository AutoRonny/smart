package com.ericsson.iec.core;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.ConsumptionRequestMessageHandler;
import com.energyict.mdus.core.RequestMessageHandler;
import com.energyict.mdus.core.ResponseMessageHandler;
import com.energyict.mdus.core.SapRequestMessage;
import com.energyict.mdus.core.SapResponseMessage;
import com.energyict.mdus.core.exception.MdusBusinessException;
import com.energyict.mdus.core.exception.MdusJmsException;
import com.energyict.mdus.core.exception.MdusSqlException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.log.messageservicelog.MessageServiceLoggerCreator;
import com.energyict.mdus.core.model.MdusWarehouse;
import com.energyict.mdus.eventmanagement.SapEvent;
import com.energyict.mdus.eventmanagement.SapEventMessageHandler;
import com.energyict.mdw.messaging.MessageService;

public final class SapMessageQueuesHelper
{
	public static final int NULL_DELAY = 0;

	public static final String DELAY_PROPERTY_NAME = "JMS_OracleDelay";

	private static Map<String, MessageServiceAndLogger> name2MessageService = new Hashtable<>();

	private SapMessageQueuesHelper()
	{
	}

	public static void sendMessageToQueue(Serializable message, String queueName, int messagePriority) throws SystemObjectNotDefined, MdusBusinessException, MdusSqlException, MdusJmsException
	{
		sendMessageToQueue(message, queueName, messagePriority, NULL_DELAY);
	}

	public static void sendMessageToQueue(Serializable message, String queueName, int messagePriority, int delayInSeconds) throws SystemObjectNotDefined, MdusBusinessException, MdusSqlException, MdusJmsException
	{
		try
		{
			MessageService messageService = getMessageService(queueName);
			ObjectMessage jmsMessage = messageService.createObjectMessage();
			jmsMessage.setObject(message);
			if ( delayInSeconds != NULL_DELAY )
			{
				jmsMessage.setIntProperty(DELAY_PROPERTY_NAME, delayInSeconds);
			}
			messageService.send(jmsMessage, Message.DEFAULT_DELIVERY_MODE, messagePriority, 24*3600*1000);
		}
		catch (BusinessException e)
		{
			// Don't know what happened, reset the cache
			resetMessageServiceCache();
			throw new MdusBusinessException(e);
		}
		catch (JMSException e)
		{
			// Don't know what happened, reset the cache
			resetMessageServiceCache();
			throw new MdusJmsException(e);
		}
		catch (SQLException e)
		{
			// Don't know what happened, reset the cache
			resetMessageServiceCache();
			throw new MdusSqlException(e);
		}
	}

	public static synchronized Map<String, MessageServiceAndLogger> resetMessageServiceCache() {
		name2MessageService = new Hashtable<>();
		return name2MessageService;
	}

	public static MessageService getMessageService(String queueName) throws SystemObjectNotDefined
	{
		MessageServiceAndLogger messageServiceAndLogger = name2MessageService.get(queueName);
		if ( messageServiceAndLogger == null )
		{
			messageServiceAndLogger = new MessageServiceAndLogger();
			name2MessageService.put(queueName, messageServiceAndLogger);
		}

		MessageService messageService = messageServiceAndLogger.getMessageService();
		if ( messageService == null )
		{
			messageService = MdusWarehouse.getCurrent().getMeteringWarehouse().getMessageServiceFactory().find(queueName);
			if ( messageService == null )
			{
				throw new SystemObjectNotDefined(MessageService.class, queueName);
			}
			messageServiceAndLogger.setMessageService(messageService);
		}
		return messageService;
	}

	public static Logger getLogger(String queueName) throws SystemObjectNotDefined
	{
		MessageServiceAndLogger messageServiceAndLogger = name2MessageService.get(queueName);
		if ( messageServiceAndLogger == null )
		{
			messageServiceAndLogger = new MessageServiceAndLogger();
			name2MessageService.put(queueName, messageServiceAndLogger);
		}

		Logger logger = messageServiceAndLogger.getLogger();
		if ( logger == null )
		{
			MessageService messageService = getMessageService(queueName);
			logger = MessageServiceLoggerCreator.createLogger(messageService);
			if ( logger == null )
			{
				throw new SystemObjectNotDefined(MessageService.class, queueName);
			}
			messageServiceAndLogger.setLogger(logger);
		}
		return logger;
	}

	public static void sendMessageToConsumptionRequestQueue(int consumptionRequestId, int messagePriority, int messageDelay) throws SystemObjectNotDefined, MdusBusinessException, MdusSqlException, MdusJmsException
	{
		sendMessageToQueue(consumptionRequestId, ConsumptionRequestMessageHandler.PROCESS_CONSUMPTION_REQUEST_QUEUE, messagePriority, messageDelay);
	}

	public static void sendMessageToResponseQueue(SapResponseMessage message, int messagePriority) throws SystemObjectNotDefined, MdusBusinessException, MdusSqlException, MdusJmsException
	{
		sendMessageToQueue(message, ResponseMessageHandler.SAP_RESPONSE_QUEUE_NAME, messagePriority);
	}

	public static void sendMessageToRequestQueue(SapRequestMessage message, int messagePriority) throws SystemObjectNotDefined, MdusBusinessException, MdusSqlException, MdusJmsException
	{
		sendMessageToQueue(message, RequestMessageHandler.SAP_REQUEST_QUEUE_NAME, messagePriority);
	}

	public static void sendMessageToEventQueue(SapEvent message, int messagePriority) throws SystemObjectNotDefined, MdusBusinessException, MdusSqlException, MdusJmsException
	{
		sendMessageToQueue(message, SapEventMessageHandler.SAP_EVENT_QUEUE_NAME, messagePriority);
	}

	private static class MessageServiceAndLogger
	{
		private MessageService messageService;
		private Logger logger;

		private MessageServiceAndLogger()
		{
			this(null, null);
		}

		private MessageServiceAndLogger(MessageService messageService, Logger logger)
		{
			this.messageService = messageService;
			this.logger = logger;
		}

		public MessageService getMessageService()
		{
			return this.messageService;
		}

		public void setMessageService(MessageService messageService)
		{
			this.messageService = messageService;
		}

		public Logger getLogger()
		{
			return this.logger;
		}

		public void setLogger(Logger logger)
		{
			this.logger = logger;
		}
	}
}
