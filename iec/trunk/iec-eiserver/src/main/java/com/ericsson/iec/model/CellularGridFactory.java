package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface CellularGridFactory extends FolderVersionWrapperFactory<CellularGrid> {

	CellularGrid findByKey(String key);
	
}
