package com.ericsson.iec.model;

import com.energyict.projects.common.model.device.DeviceWrapperFactory;

public interface DataConcentratorDeviceFactory extends DeviceWrapperFactory<DataConcentratorDevice> {
	public DataConcentratorDevice findByKey(String key);
}
