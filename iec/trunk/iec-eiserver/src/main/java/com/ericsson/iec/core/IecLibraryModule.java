package com.ericsson.iec.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.energyict.cuo.core.SystemParameterSpec;
import com.energyict.mdus.core.SapWebService;
import com.energyict.projects.AbstractLibraryModule;
import com.energyict.projects.LibraryModule;
import com.ericsson.iec.core.exception.IecExceptionReference;
import com.ericsson.iec.mdus.MdusWebservice;

public class IecLibraryModule extends AbstractLibraryModule {
	
	private static final String MODULE_NAME = "IEC";
	

	@Override
	public Collection<LibraryModule> getDependencies() {
		return Collections.emptyList();
	}

	@Override
	public String getErrorBundleName() {
		return IecExceptionReference.class.getName();
	}

	@Override
	public String getName() {
		return MODULE_NAME;
	}
	
	@Override
	public List<SystemParameterSpec> getSystemParameterSpecs() {
		List<SystemParameterSpec> specs = new ArrayList<SystemParameterSpec>();
		for (SapWebService service : MdusWebservice.values()) {
			specs.add(new IecSystemParameterSpec(service.getSapServiceRequestTypeSystemParameterSpec()));
			specs.add(new IecSystemParameterSpec(service.getSapMessageValidityPeriodSystemParameterSpec()));
			specs.add(new IecSystemParameterSpec(service.getSapMessagePrioritySystemParameterSpec()));
		}
		for (IecSystemParameters spec : IecSystemParameters.values()) {
			specs.add(spec.toParameterSpec());
		}
		
		return specs;
	}

}
