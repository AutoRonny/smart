package com.ericsson.iec.service;

import java.io.StringWriter;
import java.util.Collections;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.energyict.mdus.core.ClassInjector;
import com.energyict.mdus.core.SapResponseMessage;
import com.energyict.mdus.core.ServiceRequestCreator;
import com.energyict.mdus.core.exception.MdusBusinessException;
import com.energyict.mdus.core.exception.MdusResourceBusyException;
import com.energyict.mdus.core.exception.MdusSqlException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;
import com.energyict.mdw.service.ServiceRequest;
import com.ericsson.iec.mdus.IecClassInjector;
import com.ericsson.iec.mdus.MdusWebservice;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.RegisteredNotificationRequest;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.RegisteredNotificationRequest.Request;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.ObjectFactory;

public class RegisteredNotificationRequestBuilder {

	private ObjectFactory objectFactory;
	private ClassInjector classInjector;

	public RegisteredNotificationRequestBuilder() {
	}
	
	public SapResponseMessage build(String utilitiesDeviceId, String serialNumber, String manufactoringYear, String meterFirmware) throws SystemObjectNotDefined, SystemParameterIncorrectValue, MdusBusinessException, MdusSqlException, MdusResourceBusyException {
		
		ServiceRequest serviceRequest = new ServiceRequestCreator().createServiceRequest(MdusWebservice.REGISTERED_NOTIFICATION_REQUEST, UUID.randomUUID().toString(), Collections.emptyList(), null);
		RegisteredNotificationRequest registeredNotificationRequest = buildRegisteredNotificationRequest();
		Request request = buildRequest(utilitiesDeviceId, serialNumber, manufactoringYear, meterFirmware);
		registeredNotificationRequest.setRequest(request);
		String message = marshall(registeredNotificationRequest);
		SapResponseMessage responseMessage = new SapResponseMessage(MdusWebservice.REGISTERED_NOTIFICATION_REQUEST, message, serviceRequest.getId());
		return responseMessage;
	}
	
	private RegisteredNotificationRequest buildRegisteredNotificationRequest() {
		return getSEObjectFactory().createRegisteredNotificationRequest();
		 
	}
	
	private Request buildRequest(String utilitiesDeviceId, String serialNumber, String manufactoringYear, String meterFirmware) {
		Request request = getSEObjectFactory().createRegisteredNotificationRequestRequest();
		request.setUtilitiesDeviceID(utilitiesDeviceId);
		request.setSerialNumberID(serialNumber);
		request.setMeterManufacturingYear(manufactoringYear);
		request.setComFWVersion("not-available");
		request.setMeterFWVersion(meterFirmware);
		//request.setIP("");
		return request;
	}
	
	private String marshall(RegisteredNotificationRequest registeredNotificationRequest) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(RegisteredNotificationRequest.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			StringWriter writer = new StringWriter();

			marshaller.marshal(registeredNotificationRequest, writer);
			return writer.toString();
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private ClassInjector getClassInjector() {
		if (classInjector==null) {
			classInjector=new IecClassInjector();
		}
		return classInjector;
	}
	
	private ObjectFactory getSEObjectFactory() {
		if (objectFactory == null) {
			objectFactory = new ObjectFactory();
		}
		return objectFactory;
	}
	
	
	
	
}
