package com.ericsson.iec.model;

import java.math.BigDecimal;
import java.util.Date;

import com.energyict.coordinates.SpatialCoordinates;
import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;
import com.ericsson.iec.core.IecWarehouse;
import static com.ericsson.iec.constants.AttributeTypeConstants.TransformerAttributes.*;

public class TransformerImpl extends FolderVersionWrapperImpl implements Transformer {

	private static final long serialVersionUID = -1871659369835667823L;

	protected TransformerImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public District getDistrict() {
		return getFolderVersionWrapperAttribute(DISTRICT.name, IecWarehouse.getInstance().getDistrictFactory());
	}
	
	@Override
	public void setDistrict(District district) {
		setFolderVersionWrapperAttribute(DISTRICT.name, district);
	}

	@Override
	public Region getRegion() {
		return getFolderVersionWrapperAttribute(REGION.name, IecWarehouse.getInstance().getRegionFactory());
	}
	@Override
	public void setRegion(Region region) {
		setFolderVersionWrapperAttribute(REGION.name, region);
	}

	@Override
	public City getCity() {
		return getFolderVersionWrapperAttribute(CITY.name, IecWarehouse.getInstance().getCityFactory());
	}
	
	@Override
	public void setCity(City city) {
		setFolderVersionWrapperAttribute(CITY.name, city);
	}

	@Override
	public TransformerStation getTransformerStation() {
		return getFolderVersionWrapperAttribute(TRANSFORMER_STATION.name, IecWarehouse.getInstance().getTransformerStationFactory());
	}
	
	@Override
	public void setTransformerStation(TransformerStation transformerStation) {
		setFolderVersionWrapperAttribute(TRANSFORMER_STATION.name, transformerStation);		
	}
	
	@Override
	public String getHighVoltageLineFeeder() {
		return getStringAttribute(HIGH_VOLTAGE_LINE_FEEDER.name);
	}
	
	@Override
	public void setHighVoltageLineFeeder(String feeder) {
		setStringAttribute(HIGH_VOLTAGE_LINE_FEEDER.name, feeder);
	}

	@Override
	public String getNameHighVoltageLineFeeder() {
		return getStringAttribute(NAME_HIGH_VOLTAGE_LINE_FEEDER.name);
	}

	@Override
	public void setNameHighVoltageLineFeeder(String feederName) {
		setStringAttribute(NAME_HIGH_VOLTAGE_LINE_FEEDER.name, feederName);
	}

	@Override
	public String getManufacturer() {
		return getStringAttribute(MANUFACTURER.name);
	}
	
	@Override
	public void setManufacturer(String manufacturer) {
		setStringAttribute(MANUFACTURER.name, manufacturer);
	}
	
	public BigDecimal getSupplyVoltageAmpereRating() {
		return getBigDecimalAttribute(SUPPLY_VOLTAGE_AMPERE_RATING.name);
	}
	
	@Override
	public void setSupplyVoltageAmpereRating(BigDecimal rating) {
		setBigDecimalAttribute(SUPPLY_VOLTAGE_AMPERE_RATING.name, rating);
	}
	
	@Override
	public SpatialCoordinates getCoordinates() {
		return getSpatialCoordinatesAttribute(COORDINATES.name);
	}
	
	@Override
	public void setCoordinates(SpatialCoordinates coordinates) {
		setSpatialCoordinatesAttribute(COORDINATES.name, coordinates);
	}

	@Override
	public String getDryOil() {
		return getStringAttribute(DRY_OIL.name);
	}
	
	@Override
	public void setDryOil(String dryOil) {
		setStringAttribute(DRY_OIL.name, dryOil);
	}
	
	@Override
	public String getTransformerType() {
		return getStringAttribute(TRANSFORMER_TYPE.name);
	}
	
	@Override
	public void setTransformerType(String type) {
		setStringAttribute(TRANSFORMER_TYPE.name, type);
	}
	
	@Override
	public String getRemarks() {
		return getStringAttribute(REMARKS.name);
	}
	@Override
	public void setRemarks(String remarks) {
		setStringAttribute(REMARKS.name, remarks);
	}

	@Override
	public Date getLatestNisUpdateDate() {
		return getDateAndTimeAttribute(LATEST_NIS_UPDATE_DATE.name);
	}
	
	@Override
	public void setLatestNisUpdateDate(Date latestNisUpdateDate) {
		setDateAndTimeAttribute(LATEST_NIS_UPDATE_DATE.name, latestNisUpdateDate);
	}
}

