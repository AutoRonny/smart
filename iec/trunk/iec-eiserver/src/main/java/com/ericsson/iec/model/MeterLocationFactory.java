package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface MeterLocationFactory extends FolderVersionWrapperFactory<MeterLocation> {

	MeterLocation findByKey(String key);
}
