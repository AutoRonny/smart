package com.ericsson.iec.constants;

public enum Warehouses {
	DATA_CONCENTRATOR_WAREHOUSE("Dataconcentrator Warehouse", "DC_WAREHOUSE"),
	PLC_METER_WAREHOUSE("PLC Meter Warehouse", "PLC_METER_WAREHOUSE"),
	CELLULAR_METER_WAREHOUSE("Cellular Meter Warehouse", "CELLULAR_METER_WAREHOUSE"),
	METER_LOCATION_WAREHOUSE("Meter Location Warehouse", "METER_LOCATIONS"),
	POINT_OF_DELIVERY_WAREHOUSE("Point Of Delivery Warehouse", "POINT_OF_DELIVERY"),
	;
	
	private String name;
	private String externalName;
	
	Warehouses(String name, String externalName) {
		this.name= name;
		this.externalName = externalName;
	}

	public String getExternalName() {
		return externalName;
	}
	
	public String getName() {
		return name;
	}
}
