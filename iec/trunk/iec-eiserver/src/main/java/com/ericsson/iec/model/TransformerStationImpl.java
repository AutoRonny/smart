package com.ericsson.iec.model;

import java.math.BigDecimal;
import java.util.Date;

import com.energyict.coordinates.SpatialCoordinates;
import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;
import com.ericsson.iec.core.IecWarehouse;
import static com.ericsson.iec.constants.AttributeTypeConstants.TransformerStationAttributes.*;

public class TransformerStationImpl extends FolderVersionWrapperImpl implements TransformerStation {

	private static final long serialVersionUID = -1871659369835667822L;

	protected TransformerStationImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public District getDistrict() {
		return getFolderVersionWrapperAttribute(DISTRICT.name, IecWarehouse.getInstance().getDistrictFactory());
	}
	@Override
	public void setDistrict(District district) {
		setFolderVersionWrapperAttribute(DISTRICT.name, district);
	}

	@Override
	public Region getRegion() {
		return getFolderVersionWrapperAttribute(REGION.name, IecWarehouse.getInstance().getRegionFactory());
	}
	@Override
	public void setRegion(Region region) {
		setFolderVersionWrapperAttribute(REGION.name, region);
	}

	@Override
	public City getCity() {
		return getFolderVersionWrapperAttribute(CITY.name, IecWarehouse.getInstance().getCityFactory());
	}
	
	@Override
	public void setCity(City city) {
		setFolderVersionWrapperAttribute(CITY.name, city);
	}

	@Override
	public String getStationId() {
		return getStringAttribute(STATION_ID.name);
	}
	@Override
	public void setStationId(String stationId) {
		setStringAttribute(STATION_ID.name, stationId);
	}

	@Override
	public String getStationName() {
		return getStringAttribute(STATION_NAME.name);
	}
	@Override
	public void setStationName(String stationName) {
		setStringAttribute(STATION_NAME.name, stationName);
	}

	@Override
	public String getStationLocation() {
		return getStringAttribute(STATION_LOCATION.name);
	}
	@Override
	public void setStationLocation(String stationLocation) {
		setStringAttribute(STATION_LOCATION.name, stationLocation);
	}

	@Override
	public SpatialCoordinates getCoordinates() {
		return getSpatialCoordinatesAttribute(COORDINATES.name);
	}
	@Override
	public void setCoordinates(SpatialCoordinates coordinates) {
		setSpatialCoordinatesAttribute(COORDINATES.name, coordinates);
	}

	@Override
	public BigDecimal getStationType() {
		return getBigDecimalAttribute(STATION_TYPE.name);
	}
	@Override
	public void setStationType(BigDecimal stationType) {
		setBigDecimalAttribute(STATION_TYPE.name, stationType);
	}
	
	@Override
	public BigDecimal getNumberOfTransformers() {
		return getBigDecimalAttribute(NUMBER_OF_TRANSFORMERS.name);
	}
	@Override
	public void setNumberOfTransformers(BigDecimal numberOfTransformers) {
		setBigDecimalAttribute(NUMBER_OF_TRANSFORMERS.name, numberOfTransformers);
	}

	@Override
	public BigDecimal getNumberOfPlcTransformers() {
		return getBigDecimalAttribute(NUMBER_OF_PLC_TRANSFORMERS.name);
	}
	@Override
	public void setNumberOfPlcTransformers(BigDecimal numberOfPlcTransformers) {
		setBigDecimalAttribute(NUMBER_OF_PLC_TRANSFORMERS.name, numberOfPlcTransformers);
	}

	@Override
	public String getRemarks() {
		return getStringAttribute(REMARKS.name);
	}
	@Override
	public void setRemarks(String remarks) {
		setStringAttribute(REMARKS.name, remarks);
	}

	@Override
	public Date getLatestNisUpdateDate() {
		return super.getDateAndTimeAttribute(LATEST_NIS_UPDATE_DATE.name);
	}
	
	@Override
	public void setLatestNisUpdateDate(Date latestNisUpdateDate) {
		setDateAndTimeAttribute(LATEST_NIS_UPDATE_DATE.name, latestNisUpdateDate);
	}
}
