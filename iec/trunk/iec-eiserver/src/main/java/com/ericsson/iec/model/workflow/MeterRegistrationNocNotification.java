package com.ericsson.iec.model.workflow;

import com.energyict.mdw.service.ServiceRequest;
import com.energyict.projects.common.model.processcase.exceptionhandling.ProcessCaseWrapperWithExceptionHandling;
import com.ericsson.iec.model.GenericMeter;

public interface MeterRegistrationNocNotification extends ProcessCaseWrapperWithExceptionHandling {

	void setRegistrationStatus(String registrationStatus);
	String getRegistrationStatus();

	void setMeter(GenericMeter meter);
	GenericMeter getMeter();

	void setRetry(Boolean retry);
	Boolean getRetry();
	
	void setServiceRequest(ServiceRequest serviceRequest);
	ServiceRequest getServiceRequest();
	
}
