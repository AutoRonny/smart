package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;

public class CellularGridFactoryImpl extends FolderVersionWrapperFactoryImpl<CellularGrid> implements CellularGridFactory {

	public CellularGridFactoryImpl() {
		super();
	}
	
	public CellularGridFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	@Override
	public CellularGrid createNew(FolderVersionAdapter arg0) {
		return new CellularGridImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return FolderTypes.CELLULAR_GRID.name;
	}
	
	@Override
	public CellularGrid findByKey(String key) {
		return findByExternalName(FolderTypes.CELLULAR_GRID.buildExternalName(key), new Date());
	}
}
