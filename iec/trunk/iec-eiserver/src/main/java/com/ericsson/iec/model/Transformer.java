package com.ericsson.iec.model;

import java.math.BigDecimal;
import java.util.Date;

import com.energyict.coordinates.SpatialCoordinates;
import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface Transformer extends FolderVersionWrapper {

	District getDistrict();
	void setDistrict(District district);

	Region getRegion();
	void setRegion(Region region);
	
	City getCity();
	void setCity(City city);
	
	TransformerStation getTransformerStation();
	void setTransformerStation(TransformerStation transformerStation);

	String getHighVoltageLineFeeder();
	void setHighVoltageLineFeeder(String feeder);
	
	String getNameHighVoltageLineFeeder();
	void setNameHighVoltageLineFeeder(String feederName);
	
	SpatialCoordinates getCoordinates();
	void setCoordinates(SpatialCoordinates coordinates);

	String getManufacturer();
	void setManufacturer(String manufacturer);
	
	BigDecimal getSupplyVoltageAmpereRating();
	void setSupplyVoltageAmpereRating(BigDecimal rating);
	
	String getDryOil();
	void setDryOil(String dryOil);
	
	String getTransformerType();
	void setTransformerType(String type);
	
	String getRemarks();
	void setRemarks(String remarks);

	Date getLatestNisUpdateDate();
	void setLatestNisUpdateDate(Date latestNisUpdateDate);
}

