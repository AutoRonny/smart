package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.MeteringWarehouse;
import com.energyict.projects.common.model.device.DeviceAdapter;
import com.energyict.projects.common.model.device.DeviceWrapperFactoryImpl;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;

public class DataConcentratorDeviceFactoryImpl extends DeviceWrapperFactoryImpl<DataConcentratorDevice> implements DataConcentratorDeviceFactory {

	public DataConcentratorDeviceFactoryImpl() {
		super(new IecDeviceAdapterFactory());
	}
	
	@Override
	public DataConcentratorDevice createNew(DeviceAdapter adapter) {
		return new DataConcentratorDeviceImpl(adapter);
	}

	@Override
	public DataConcentratorDevice findByKey(String key) {
		Device device = MeteringWarehouse.getCurrent().getDeviceFactory().findByExternalName(FolderTypes.DATA_CONCENTRATOR.buildExternalName(key));
		return super.createNew(device, new Date(0));
	}
}
