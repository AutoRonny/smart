package com.ericsson.iec.prototype;

import static com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage.PROTOTYPE_NOT_FOUND;
import static com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage.TOO_MANY_PROTOTYPES_FOUND;

import java.util.List;

import com.energyict.mdw.template.CopySpec;
import com.ericsson.iec.constants.ProtoTypeConstants.ProtoTypeObjectType;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.FatalConfigurationException;

public abstract class AbstractProtoTypeFactoryImpl {

	public AbstractProtoTypeFactoryImpl()  {
	}
	
	protected CopySpec getCopySpec() throws FatalConfigurationException {
		List<CopySpec> copySpecs = IecWarehouse.getInstance().getMeteringWarehouse().getCopySpecFactory().findByName(getCopySpecName());
		if (copySpecs.isEmpty()) {
			throw new FatalConfigurationException(PROTOTYPE_NOT_FOUND, getCopySpecName());
		}
		if (copySpecs.size()>1) {
			throw new FatalConfigurationException(TOO_MANY_PROTOTYPES_FOUND, getCopySpecName());
		}
		return copySpecs.get(0);
		
	}
	
	public abstract String getCopySpecName();
}
