package com.ericsson.iec.model;

import com.energyict.projects.common.model.processcase.ProcessCaseAdapter;
import static com.ericsson.iec.constants.AttributeTypeConstants.ProcessCaseWithErrorHandlingAttributes.*;
import com.energyict.projects.common.model.processcase.ProcessCaseWrapperImpl;
import com.energyict.projects.common.model.processcase.exceptionhandling.ProcessCaseWrapperWithExceptionHandling;

public abstract class ProcessCaseWithExceptionHandling extends ProcessCaseWrapperImpl implements ProcessCaseWrapperWithExceptionHandling {

	private static final long serialVersionUID = 3309616484284985651L;

	protected ProcessCaseWithExceptionHandling(ProcessCaseAdapter adapter) {
		super(adapter);
	}

	@Override
	public Boolean getTransitionSuccess() {
		return getBooleanAttribute(TRANSITION_SUCCESS.name);
	}

	@Override
	public void setTransitionSuccess(Boolean transitionSuccess) {
		setBooleanAttribute(TRANSITION_SUCCESS.name, transitionSuccess);
		
	}

	@Override
	public String getErrorMessage() {
		return getStringAttribute(ERROR_MESSAGE.name);
	}

	@Override
	public void setErrorMessage(String errorMessage) {
		setStringAttribute(ERROR_MESSAGE.name, errorMessage);
		
	}

	@Override
	public String getTransitionName() {
		return getStringAttribute(TRANSITION_NAME.name);
	}

	@Override
	public void setTransitionName(String transitionName) {
		setStringAttribute(TRANSITION_NAME.name, transitionName);
		
	}

}
