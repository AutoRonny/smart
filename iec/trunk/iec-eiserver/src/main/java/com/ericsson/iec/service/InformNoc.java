package com.ericsson.iec.service;

import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeFactory;
import com.energyict.mdus.core.model.MdusWarehouse;
import com.energyict.mdus.core.model.folder.EndpointConfig;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;
import com.ericsson.iec.core.exception.FatalConfigurationException;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;

import static com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage.*;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.*;
import com.ericsson.iec.model.DataConcentratorDeployment;

import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.DCFaultNotificationRequest;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.DCFaultNotificationResponse;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.ObjectFactory;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01PortType;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01ServicesServiceagent;


public class InformNoc extends AbstractServiceTransitionHandlerWithExceptionHandling<DataConcentratorDeployment>{

	public static final String TRANSITION_NAME = "Inform NOC";
	private static final String ENDPOINTTOKEN = "WSMDMNOC01";
		
	public InformNoc(Logger logger) {
		super(logger);
	}

	@Override
	protected void doProcess(DataConcentratorDeployment dataConcentratorDeployment) throws IecException {
		try {
			String endPoint = getEndPoint();
			getLogger().info("using endpoint : "+endPoint);
			WSMDMNOC01ServicesServiceagent service = new WSMDMNOC01ServicesServiceagent(new URL(endPoint));
			WSMDMNOC01PortType port = service.getWSMDMNOC01PortTypeEndpoint();
			
			ObjectFactory factory = new ObjectFactory();			
			DCFaultNotificationRequest request = factory.createDCFaultNotificationRequest();			
			request.setRequest(factory.createDCFaultNotificationRequestRequest());
			request.getRequest().setActivityStep("TO DO");
			request.getRequest().setDCID(dataConcentratorDeployment.getDataConcentrator().getSerialId());
			request.getRequest().setEventCode(dataConcentratorDeployment.getDeploymentStatus());
			
			DCFaultNotificationResponse response = port.dcFaultNotificationOperation(request);		
			if (response==null) {
				getLogger().info("Response NULL");
			} else {
				getLogger().info(response.getResponse().toString());
				JAXBContext jaxbContext = JAXBContext.newInstance(DCFaultNotificationResponse.class);
				Marshaller marshaller = jaxbContext.createMarshaller();
				StringWriter writer = new StringWriter();
				marshaller.marshal(response, writer);
				getLogger().info(writer.toString());
			}
		} catch (IecException e) {
			throw e;
		} catch(Exception e) {
			e.printStackTrace();
			throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "", "");
		}
	}
	
	private String getEndPoint() throws FatalConfigurationException {
		EndpointConfig config = MdusWarehouse.getCurrent().getEndPointConfigFactory()
			.findByExternalName(ENDPOINTTOKEN, true); // endpoint external id is epc/ + ENDPOINTTOKEN
		if (config == null) {
			throw new FatalConfigurationException(ENDPOINT_NOT_CONFIGUTED, "DCFault");
		} else {
			return config.getUrlString();
		}			
	}

	@Override
	protected String getTransitionName() {
		return TRANSITION_NAME;
	}

}
