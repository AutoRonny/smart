package com.ericsson.iec.model.workflow;

import com.energyict.projects.common.model.processcase.ProcessCaseAdapter;
import com.energyict.projects.common.model.processcase.ProcessCaseWrapperFactoryImpl;
import com.ericsson.iec.constants.ProcessConstants;

public class MeterRegistrationNocNotificationFactoryImpl extends ProcessCaseWrapperFactoryImpl<MeterRegistrationNocNotification> 
			 									   implements MeterRegistrationNocNotificationFactory {

	@Override
	public String getProcessName() {
		return ProcessConstants.Processes.METER_REGISTRATION_NOC_NOTIFICATION.name;
	}

	@Override
	public MeterRegistrationNocNotification createNew(ProcessCaseAdapter adapter) {
		return new MeterRegistrationNocNotificationImpl(adapter);
	}

}
