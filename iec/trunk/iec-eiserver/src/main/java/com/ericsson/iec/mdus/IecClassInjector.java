package com.ericsson.iec.mdus;

import com.energyict.mdus.core.ClassInjector;
import com.energyict.mdus.core.MdusClassInjector;
import com.energyict.mdus.core.exception.MdusInstantiationException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;

public class IecClassInjector extends MdusClassInjector implements ClassInjector {
	
	public IecConsumptionRequestValidityPeriodCalculator getIecConsumptionRequestValidityPeriodCalculator() 
			throws SystemObjectNotDefined, MdusInstantiationException, SystemParameterIncorrectValue {
		return new IecConsumptionRequestValidityPeriodCalculatorImpl();
	}
}
