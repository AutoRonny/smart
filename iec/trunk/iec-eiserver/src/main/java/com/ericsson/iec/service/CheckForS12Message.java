package com.ericsson.iec.service;

import java.util.logging.Logger;

import com.energyict.mdc.messages.DeviceMessage;
import com.energyict.mdc.messages.DeviceMessageFactoryImpl;
import com.energyict.mdc.messages.DeviceMessageStatus;
import com.energyict.mdw.amr.Register;
import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.DeviceEventFilter;
import com.energyict.mdw.core.DeviceMessageFilter;
import com.energyict.obis.ObisCode;
import com.ericsson.iec.model.DataConcentratorDeployment;

public class CheckForS12Message {
	
	public static final String TRANSITION_NAME = "Check For S12";
	
	private Logger logger;
	
	public CheckForS12Message(Logger logger) {
		this.logger = logger;
	}

	public final boolean process(DataConcentratorDeployment dataConcentratorDeployment) {
		Device device = dataConcentratorDeployment.getDataConcentrator().getDevice().getDevice();
		Register register = device.getRegister(new ObisCode(0,0,128,8,0,4));
		return !register.getReadings().isEmpty();
	}

}
