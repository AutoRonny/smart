package com.ericsson.iec.mdus.handler.consumptionrequest;

import static com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage.OBJECT_DOES_NOT_HAVE_VIRTUAL_METER;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_CREATE_OBJECT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_UPDATE_OBJECT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.MESSAGE_IS_EMPTY;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.OBJECT_DOES_NOT_EXISTS;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.OVERLAPPING_POD;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import javax.xml.datatype.XMLGregorianCalendar;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.model.relation.ConsumptionRequestSpecificAttributes;
import com.energyict.mdw.core.MeterPortfolio;
import com.energyict.mdw.core.TimeSeries;
import com.energyict.mdw.core.VirtualMeter;
import com.ericsson.iec.constants.Warehouses;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.AbstractConfigurationException;
import com.ericsson.iec.core.exception.AbstractValidationException;
import com.ericsson.iec.core.exception.FatalConfigurationException;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.FatalValidationException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.core.exception.NonFatalValidationException;
import com.ericsson.iec.mdus.IecMarshallerHelper;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.PodAssignment;
import com.ericsson.iec.model.PointOfDelivery;
import com.ericsson.iec.model.Warehouse;
import com.ericsson.iec.prototype.PointOfDeliveryProtoType;

import sapmdm01.interfaces.mdm.iec.MeterPodRequest;
import sapmdm01.interfaces.mdm.iec.MeterPodRequest.MessageContent;

public class MeterPodConsumptionRequestHandler extends AbstractIecConsumptionRequestHandler<MeterPodRequest> {

	@Override
	protected MeterPodRequest unmarshal(String request) throws MarshallingException {
		return (MeterPodRequest) IecMarshallerHelper.getInstance().unmarshall(request, MeterPodRequest.class);
	}
	
	@Override
	protected void doProcess(MeterPodRequest message, Logger logger, ConsumptionRequestSpecificAttributes attributes) throws IecException {
		MessageContent requestContent = message.getMessageContent();
		ProcessContext processContext = new ProcessContext(requestContent);
		RequestValidator.validateMessageContent(requestContent, processContext);		
		RequestProcessor.processMessageContent(requestContent, processContext, logger);
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class ProcessContext {
		Date startDate;
		Date endDate;
		String smartMeter;
		String deviceId;
		String podId;
		
		GenericMeter meter;
		PointOfDelivery pod;
		PointOfDeliveryProtoType podProtoType;
		PodAssignment podAssignment;
		List<PodAssignment> podAssignmentList;
		Warehouse podWarehouse;
		
		public ProcessContext(MessageContent requestContent) {
			initialize(requestContent);
		}
		
		private void initialize(MessageContent requestContent) {
			if (requestContent == null)
				return;
			
			XMLGregorianCalendar xmlStartDate = requestContent.getStartDateTime();
			XMLGregorianCalendar xmlEndDate = requestContent.getEndDateTime();
			
			startDate = (xmlStartDate != null) ? xmlStartDate.toGregorianCalendar().getTime() : null;
			endDate = (xmlEndDate != null) ? xmlEndDate.toGregorianCalendar().getTime() : null;
			deviceId = requestContent.getUtilitiesDevice();
			podId = requestContent.getUtilitiesPointOfDeliveryPartyID();
			smartMeter = requestContent.getSmartMeter();
			
			meter = CommonObjectFactory.getMeter(deviceId);
			pod = CommonObjectFactory.getPointOfDelivery(podId);
			podAssignmentList = CommonObjectFactory.getPodAssignment(meter);
			podAssignment = getExactPodAssignment(podAssignmentList, startDate, endDate);
		}

		private PodAssignment getExactPodAssignment(List<PodAssignment> podAsignmentList, Date startDate, Date endDate) {
			for (PodAssignment p : podAssignmentList) {
				if (startDate.compareTo(p.getFrom()) == 0 && endDate.compareTo(p.getTo()) == 0)
					return p;
			}
			return null;
		}
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class RequestValidator {
		public static void validateMessageContent(MessageContent requestContent, ProcessContext pc) throws AbstractValidationException, AbstractConfigurationException {
			if (requestContent == null) 
				throw new FatalValidationException(MESSAGE_IS_EMPTY);

			// Validate Fields:
			CommonValidatorFactory.validateMandatoryValue(pc.startDate, "Start Date");
			CommonValidatorFactory.validateMandatoryValue(pc.endDate, "End Date");
			CommonValidatorFactory.validateValue(pc.endDate, "31/12/9999", "EndDate");
			CommonValidatorFactory.validateValue(pc.smartMeter, "MDMA", "UtilitiesAdvancedMeteringSystemID");
			
			pc.podWarehouse = IecWarehouse.getInstance().getWarehouseFactory().find(Warehouses.POINT_OF_DELIVERY_WAREHOUSE); // contains validation for null
			
			validateMeter(pc);
			validatePodAssignment(pc);
		}
		
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		private static void validateMeter(ProcessContext pc) throws NonFatalValidationException {
			if (pc.meter == null) 
				throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Meter", pc.deviceId);
		}
		
		private static void validatePodAssignment(ProcessContext pc) throws AbstractValidationException {
			int exactPodAssignmentId = (pc.podAssignment != null) ? pc.podAssignment.getId() : -1;
			
			for (PodAssignment p : pc.podAssignmentList) {
				if (pc.startDate.compareTo(p.getTo()) <= 0 && pc.endDate.compareTo(p.getFrom()) >= 0 && p.getId() != exactPodAssignmentId) 	// Overlap, Not Equal
					throw new NonFatalValidationException(OVERLAPPING_POD, pc.pod.getName(), pc.meter.getName(), pc.meter.getExternalName(), pc.startDate, pc.endDate);
			}
		}
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class RequestProcessor {
		public static void processMessageContent(MessageContent requestContent, ProcessContext pc, Logger logger) throws IecException {
			if (pc.pod == null) {
				createPod(pc);
				logger.info("Point Of Delivery Created");
			}

			setPodAssignment(pc);
			logger.info("Pod Assignment Created");
			
			setPodPortfolio(pc);
			logger.info("Pod Portfolio Updated");
			
			logger.info("Message Processing Completed");
		}

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		private static void createPod(ProcessContext pc) throws IecException {
			try {
				pc.podProtoType = IecWarehouse.getInstance().getPointOfDeliveryProtoTypeFactory().find();
				
				pc.podProtoType.setPodId(pc.podId);
				pc.podProtoType.setWarehouse(pc.podWarehouse);
				pc.podProtoType.setActiveDate(new Date(0));
			} catch (Exception e) {
				throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Point Of Delivery", pc.podId);
			}
			
			try {
				pc.pod = pc.podProtoType.save();
			} catch (BusinessException | SQLException e) {
				throw new FatalExecutionException(e, CANNOT_CREATE_OBJECT, "Point Of Delivery", pc.podId);
			}
		}

		private static void setPodAssignment(ProcessContext pc) throws IecException {
			boolean isNewPodAssignment = (pc.podAssignment == null);
			if (!isNewPodAssignment)
				return; // Rein requested to prevent updates
			
			if (pc.podAssignment == null)
				pc.podAssignment = IecWarehouse.getInstance().getPodAssignmentFactory().createNew();

			try {
				pc.podAssignment.setMeter(pc.meter);
				pc.podAssignment.setPointOfDelivery(pc.pod);
				pc.podAssignment.setUniqueIdentifier(UUID.randomUUID().toString());
				pc.podAssignment.setActiveDate(pc.startDate);
				pc.podAssignment.setFrom(pc.startDate);
				pc.podAssignment.setTo(pc.endDate);
				
				pc.podAssignment.saveChanges();
			} catch (BusinessException | SQLException e) {
				if (isNewPodAssignment)
					throw new FatalExecutionException(e, CANNOT_CREATE_OBJECT, "Pod Assignment", pc.pod.getExternalName());
				else
					throw new FatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Pod Assignment", pc.pod.getExternalName());
			}
		}

		private static void setPodPortfolio(ProcessContext pc) throws IecException {
			VirtualMeter virtualMeter = pc.meter.getVirtualMeter();
			VirtualMeter podVirtualMeter = pc.pod.getVirtualMeter();

			if (virtualMeter == null)
				throw new FatalConfigurationException(OBJECT_DOES_NOT_HAVE_VIRTUAL_METER, "Meter", pc.meter.getExternalName());
			if (podVirtualMeter == null)
				throw new FatalConfigurationException(OBJECT_DOES_NOT_HAVE_VIRTUAL_METER, "Point Of Delivery", pc.pod.getExternalName());

			try {
				MeterPortfolio podPortfolio = podVirtualMeter.getPortfolio();
				podPortfolio.add((TimeSeries)virtualMeter, pc.startDate, pc.endDate);
			} catch (BusinessException | SQLException e) {
				throw new FatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Pod Portfolio", pc.pod.getExternalName());
			}
		}
	}
}
