package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface Region extends FolderVersionWrapper {

	District getDistrict();
	void setDistrict(District district);

	Date getLatestNisUpdateDate();
	void setLatestNisUpdateDate(Date latestNisUpdateDate);
}
