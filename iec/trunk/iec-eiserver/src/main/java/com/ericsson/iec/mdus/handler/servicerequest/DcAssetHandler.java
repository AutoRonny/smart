package com.ericsson.iec.mdus.handler.servicerequest;

import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.energyict.cbo.BusinessException;
import com.energyict.cbo.TimePeriod;
import com.energyict.mdus.core.SapRequestMessage;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.exception.MdusInstantiationException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;
import com.energyict.mdus.core.requesthandler.RequestHandlerParameters;
import com.energyict.mdw.shadow.imp.ConsumptionRequestShadow;
import com.ericsson.iec.core.IecWarehouse;

import nismdm01.interfaces.mdm.iec.TransformerStationRequest;
import nocmdm01.interfaces.mdm.iec.DCAssetRequest;

public class DcAssetHandler extends IecSingleRequestHandler<DCAssetRequest>{

	public DcAssetHandler() throws MarshallingException {
		super();
	}

	@Override
	public void handle(SapRequestMessage sapRequestMessage) throws BusinessException {
		DCAssetRequest request = unmarshall(sapRequestMessage.getMessage(), DCAssetRequest.class);
		String uuid = request.getMessageHeader().getMessageID();
		RequestHandlerParameters<DCAssetRequest> parameters = 
				new RequestHandlerParameters<DCAssetRequest>(sapRequestMessage, request, uuid);
		handle(parameters);
	}
	
	protected ConsumptionRequestShadow getConsumptionRequestShadow(RequestHandlerParameters<DCAssetRequest> parameters) throws BusinessException {
		DCAssetRequest singleRequest = parameters.getRequest();
		try {
			return buildConsumptionRequestShadow(singleRequest);
		} catch (JAXBException e) {
			throw new BusinessException(e);
		}
		
	}

	private ConsumptionRequestShadow buildConsumptionRequestShadow(DCAssetRequest singleRequest) throws SystemObjectNotDefined, MdusInstantiationException, SystemParameterIncorrectValue, JAXBException {
		ConsumptionRequestShadow shadow = new ConsumptionRequestShadow();
		TimePeriod validityPeriod = IecWarehouse.getInstance().getIecClassInjector().getIecConsumptionRequestValidityPeriodCalculator().calculateForDcAssetRequest(singleRequest);
		
		shadow.setName(singleRequest.getMessageContent().getDCID().toString());
		shadow.setExternalName(singleRequest.getMessageContent().getDCID().toString());
		shadow.setFrom(validityPeriod.getFrom());
		shadow.setTo(validityPeriod.getTo());
		
		JAXBContext jaxbContext = JAXBContext.newInstance(DCAssetRequest.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		StringWriter writer = new StringWriter();
		marshaller.marshal(singleRequest, writer);
		shadow.setRequest(writer.toString()); 
		
				
		return shadow;
	}

}
