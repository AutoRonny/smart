package com.ericsson.iec.service.meterregistration;

import java.util.logging.Logger;

import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.model.workflow.MeterRegistrationNocNotification;

public class CreateNocNotificationServiceRequest extends AbstractServiceTransitionHandlerWithExceptionHandling<MeterRegistrationNocNotification>{

	public static final String TRANSITION_NAME = "CreateNocNotificationServiceRequest";
			
	public CreateNocNotificationServiceRequest(Logger logger) {
		super(logger);
	}

	@Override
	protected void doProcess(MeterRegistrationNocNotification meterRegistrationNocNotification) throws IecException {
		meterRegistrationNocNotification.setRegistrationStatus("0");
	}
	
	@Override
	protected String getTransitionName() {
		return TRANSITION_NAME;
	}

}
