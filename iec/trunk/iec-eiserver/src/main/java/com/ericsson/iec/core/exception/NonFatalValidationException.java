package com.ericsson.iec.core.exception;

import com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage;

public class NonFatalValidationException extends AbstractValidationException {

	private static final long serialVersionUID = -4161286101151051217L;

	public NonFatalValidationException(ValidationExceptionMessage exceptionCode, Object... messageParameters) {
		super(exceptionCode, false, messageParameters);
	}

	public NonFatalValidationException(Throwable cause, ValidationExceptionMessage exceptionCode, Object... messageParameters) {
		super(cause, exceptionCode, false, messageParameters);
	}
}
