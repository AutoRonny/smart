package com.ericsson.iec.model;

import static com.ericsson.iec.constants.FolderTypeConstants.FolderTypes.POD_ASSIGNMENT;

import java.util.ArrayList;
import java.util.List;

import com.energyict.mdw.relation.CompositeFilterCriterium;
import com.energyict.mdw.relation.FilterAspect;
import com.energyict.mdw.relation.Relation;
import com.energyict.mdw.relation.RelationDynamicAspect;
import com.energyict.mdw.relation.RelationSearchFilter;
import com.energyict.mdw.relation.RelationType;
import com.energyict.mdw.relation.SimpleFilterCriterium;
import com.energyict.projects.common.model.folder.FolderVersionWrapper;
import com.energyict.projects.common.model.relation.RelationAdapter;
import com.energyict.projects.common.model.relation.RelationWrapperFactoryImpl;

public class PodAssignmentFactoryImpl extends RelationWrapperFactoryImpl<PodAssignment> implements PodAssignmentFactory {

	@Override
	public PodAssignment createNew(RelationAdapter adapter) {
		return new PodAssignmentImpl(adapter);
	}

	@Override
	protected String getRelationTypeName() {
		return POD_ASSIGNMENT.name;
	}

	@Override
	public List<PodAssignment> findByReferences(GenericMeter meter, FolderVersionWrapper pointOfDelivery) {
		List<PodAssignment> result = new ArrayList<PodAssignment>();
		
		FilterAspect aspectPointOfDelivery = new RelationDynamicAspect(getRelationType().getAttributeType("pointOfDelivery"));
		FilterAspect aspectMeter = new RelationDynamicAspect(getRelationType().getAttributeType("meter"));
    
		SimpleFilterCriterium pointOfDeliveryCriterium = new SimpleFilterCriterium(aspectPointOfDelivery, SimpleFilterCriterium.OPERATOR_EQUALS, pointOfDelivery.getFolder());
		SimpleFilterCriterium meterCriterium = new SimpleFilterCriterium(aspectMeter, SimpleFilterCriterium.OPERATOR_EQUALS, meter.getFolder());
		
		CompositeFilterCriterium allCriterium = new CompositeFilterCriterium(CompositeFilterCriterium.AND_OPERATOR);
		allCriterium.add(pointOfDeliveryCriterium);
		allCriterium.add(meterCriterium);
		
		RelationSearchFilter filter = new RelationSearchFilter(allCriterium);
		for (Relation relation : getRelationType().findByFilter(filter)) {
			result.add(createNew(relation, relation.getFrom()));
		}
		return result;
	}
  
	@Override
	public List<PodAssignment> findByReferences(GenericMeter meter) {
		List<PodAssignment> result = new ArrayList<PodAssignment>();
		
		if (meter != null) {
			FilterAspect aspectMeter = new RelationDynamicAspect(getRelationType().getAttributeType("meter"));
	    
			SimpleFilterCriterium meterCriterium = new SimpleFilterCriterium(aspectMeter, SimpleFilterCriterium.OPERATOR_EQUALS, meter.getFolder());
			
			RelationSearchFilter filter = new RelationSearchFilter(meterCriterium);
			RelationType relationType = getRelationType();
			
			for (Relation relation : relationType.findByFilter(filter)) {
				result.add(createNew(relation, relation.getFrom()));
			}
		}
		return result;
	}
  
	@Override
	public List<PodAssignment> findByReferences(String uniqueId) {
		List<PodAssignment> result = new ArrayList<PodAssignment>();
		
		if (uniqueId != null) {
			FilterAspect aspectUnique = new RelationDynamicAspect(getRelationType().getAttributeType("uniqueIdentifier"));
			        
			SimpleFilterCriterium aspectUniqueIdCriterium = new SimpleFilterCriterium(aspectUnique, SimpleFilterCriterium.OPERATOR_EQUALS, uniqueId);
			        
			RelationSearchFilter filter = new RelationSearchFilter(aspectUniqueIdCriterium);
			for (Relation relation : getRelationType().findByFilter(filter)) {
				result.add(createNew(relation, relation.getFrom()));
			}
		}
		return result;
	}
}
