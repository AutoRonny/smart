package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;

public class CityFactoryImpl extends FolderVersionWrapperFactoryImpl<City> implements CityFactory {

	public CityFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	public CityFactoryImpl() {
		super();
	}

	@Override
	public City createNew(FolderVersionAdapter arg0) {
		return new CityImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return FolderTypes.CITY.name;
	}
	
	@Override
	public City findByKey(String key) {
		return findByExternalName(FolderTypes.CITY.buildExternalName(key), new Date());
	}
}
