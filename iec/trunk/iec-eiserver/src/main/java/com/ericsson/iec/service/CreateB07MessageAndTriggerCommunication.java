package com.ericsson.iec.service;

import static com.energyict.protocolimplv2.messages.STGReportsMessages.B07_2;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeFactory;

import com.energyict.cbo.BusinessException;
import com.energyict.cpo.TypedProperties;
import com.energyict.mdc.messages.DeviceMessage;
import com.energyict.mdc.messages.DeviceMessageStatus;
import com.energyict.mdc.shadow.messages.DeviceMessageShadow;
import com.energyict.mdus.core.model.MdusWarehouse;
import com.energyict.mdus.core.model.folder.EndpointConfig;
import com.energyict.mdw.core.UserFile;
import com.energyict.mdwswing.util.DeviceMessageTaskTrigger;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.FatalConfigurationException;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;

import static com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage.*;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.*;
import com.ericsson.iec.model.DataConcentratorDeployment;

import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.DCFaultNotificationRequest;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.DCFaultNotificationResponse;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.ObjectFactory;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01PortType;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01ServicesServiceagent;


public class CreateB07MessageAndTriggerCommunication extends AbstractServiceTransitionHandlerWithExceptionHandling<DataConcentratorDeployment>{

	private static final String PROPERTY_XML_FILE = "STGReportsMessages.DC_TASKS_XMLFILE";
	private static final String PROPERTY_PRIORITY = "STGReportsMessages.OrderRequest.Priority";
	private static final String PRIORITY_VERY_HIGH = "Very High";

	public static final String TRANSITION_NAME = "CreateB07MessageAndTriggerCommunication";
	
	private Integer userFileId;
	
	public CreateB07MessageAndTriggerCommunication(Logger logger, Integer userFileId) {
		super(logger);
		this.userFileId = userFileId;
	}

	@Override
	protected void doProcess(DataConcentratorDeployment dataConcentratorDeployment) throws IecException {
		try {
			String trackingId = UUID.randomUUID().toString();
		
			DeviceMessage message = dataConcentratorDeployment.getDataConcentrator().getDevice().getDevice().createMessage(buildShadow(trackingId));
			DeviceMessageTaskTrigger trigger = new DeviceMessageTaskTrigger();
			trigger.triggerTask(Arrays.asList(message));
		
			dataConcentratorDeployment.setTrackingId(trackingId);
		}
		catch (BusinessException | SQLException e) {
			e.printStackTrace();
			throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "", "");
		}

	}

	private DeviceMessageShadow buildShadow(String trackingId) {
		DeviceMessageShadow shadow = new DeviceMessageShadow();
		shadow.setDeviceMessageSpecPrimaryKey(B07_2.getPrimaryKey());
		shadow.setDeviceMessageStatus(DeviceMessageStatus.PENDING);
		shadow.setProperties(buildProperties());
		shadow.setTrackingId(trackingId);
		return shadow;
		
	}
	
	private TypedProperties buildProperties() {
		TypedProperties typedProperties = TypedProperties.empty();
		typedProperties.setProperty(PROPERTY_PRIORITY, PRIORITY_VERY_HIGH);
		typedProperties.setProperty(PROPERTY_XML_FILE, getUserFile());
		return typedProperties;
	}
	
	private UserFile getUserFile() {
		return IecWarehouse.getInstance().getMeteringWarehouse().getUserFileFactory().find(userFileId);
	}
	
	@Override
	protected String getTransitionName() {
		return TRANSITION_NAME;
	}

}
