package com.ericsson.iec.model;

import static com.ericsson.iec.constants.AttributeTypeConstants.PointOfDeliveryAttributes.VIRTUAL_METER;

import com.energyict.mdw.core.VirtualMeter;
import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;

public class PointOfDeliveryImpl extends FolderVersionWrapperImpl implements PointOfDelivery {

	private static final long serialVersionUID = -1871659369835667823L;

	protected PointOfDeliveryImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public VirtualMeter getVirtualMeter() {
		return getVirtualMeterAttribute(VIRTUAL_METER.name);
	}
	@Override
	public void setVirtualMeter(VirtualMeter virtualMeter) {
		setVirtualMeterAttribute(VIRTUAL_METER.name, virtualMeter);
	}
}

