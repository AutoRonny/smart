package com.ericsson.iec.prototype;

import com.ericsson.iec.model.PointOfDelivery;

public interface PointOfDeliveryProtoType extends ProtoType<PointOfDelivery> {

	void setPodId(String podId);
}
