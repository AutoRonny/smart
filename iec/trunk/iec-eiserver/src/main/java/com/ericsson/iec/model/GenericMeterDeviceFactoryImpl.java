package com.ericsson.iec.model;

import com.energyict.projects.common.model.device.DeviceAdapter;
import com.energyict.projects.common.model.device.DeviceWrapperFactoryImpl;

public class GenericMeterDeviceFactoryImpl extends DeviceWrapperFactoryImpl<GenericMeterDevice> implements GenericMeterDeviceFactory {

	public GenericMeterDeviceFactoryImpl() {
		super(new IecDeviceAdapterFactory());
	}
	
	@Override
	public GenericMeterDevice createNew(DeviceAdapter adapter) {
		return new GenericMeterDeviceImpl(adapter);
	}
}
