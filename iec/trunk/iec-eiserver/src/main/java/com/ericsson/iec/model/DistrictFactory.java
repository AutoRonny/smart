package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface DistrictFactory extends FolderVersionWrapperFactory<District> {

	District findByKey(String key);
	
}
