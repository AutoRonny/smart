package com.ericsson.iec.exporter.meterkey;

import com.energyict.mdc.protocol.security.SecurityPropertySet;
import com.energyict.mdc.shadow.protocol.security.SecurityProperties;
import com.energyict.mdw.core.Device;

import java.util.ArrayList;
import java.util.List;

class MeterKeyProcessor {

    public MeterKeyItem process(String serialNumberConcentrator, List<Device> devices) {
        try {
            return buildMeterKeyItem(serialNumberConcentrator, devices);
        } catch (Exception ex) {
            return null;
        }
    }

    private MeterKeyItem buildMeterKeyItem(String serialNumberConcentrator, List<Device> devices) {
        return new MeterKeyItem(serialNumberConcentrator, buildModels(devices));
    }

    private List<MeterKeyModel> buildModels(List<Device> devices) {
        List<MeterKeyModel> models = new ArrayList<>();
        for (Device meter : devices) {
            models.add(buildModel(meter));
        }
        return models;
    }

    private MeterKeyModel buildModel(Device device) {
        MeterKeyModel model = new MeterKeyModel();
        model.setSerialNumberMeter(device.getSerialNumber());
        for (SecurityProperties securityProperties : device.getShadow().getSecurityProperties()) {
            if (securityProperties.getProperties().size() > 0) {
                for (SecurityPropertySet securityPropertySet : device.getConfiguration().getCommunicationConfiguration().getSecurityPropertySets()) {
                    if (securityProperties.getSecurityPropertySetId() == securityPropertySet.getId()) {
                        model.setAuthenticationKey((String) securityProperties.get("AuthenticationKey"));
                        model.setEncryptionKey((String) securityProperties.get("EncryptionKey"));
                    }
                }
            }
        }

        return model;
    }
}
