package com.ericsson.iec.pluggable.message.handler;

import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.energyict.cbo.BusinessException;
import com.energyict.coordinates.DegreesWorldCoordinate;
import com.energyict.coordinates.SpatialCoordinates;
import com.energyict.cpo.PropertySpec;
import com.energyict.cpo.TypedProperties;
import com.energyict.mdw.messaging.MessageHandler;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.Transformer;
import com.ericsson.iec.model.TransformerFactory;
import com.ericsson.iec.model.TransformerStation;
import com.ericsson.iec.model.TransformerStationFactory;

import nismdm01.interfaces.mdm.iec.TransformerRequest;
import nismdm01.interfaces.mdm.iec.TransformerRequest.MessageContent;
import oracle.jms.AQjmsTextMessage;

@Deprecated
public class TransformerMessageHandler implements MessageHandler {

	public TransformerMessageHandler() {
	}

	@Override
	public void addProperties(TypedProperties arg0) {
	}

	@Override
	public String getVersion() {
		return "1.0";
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List<PropertySpec> getOptionalProperties() {
		return Collections.emptyList();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List<PropertySpec> getRequiredProperties() {
		return Collections.emptyList();
	}

	@Override
	public void processMessage(Message message, Logger logger) throws JMSException, BusinessException, SQLException {
		TransformerRequest request;
		MessageContent requestContent;
		
		if (message instanceof oracle.jms.AQjmsTextMessage) {
			AQjmsTextMessage textMessage = (AQjmsTextMessage)message;
			logger.info("Got Message " + textMessage.getText());
			
			JAXBContext jaxbContext;
			try {
				jaxbContext = JAXBContext.newInstance(TransformerRequest.class);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				StringReader reader = new StringReader(textMessage.getText());
				request = (TransformerRequest)unmarshaller.unmarshal(reader);
			} catch (JAXBException e) {
				logger.severe("Failed unmarshaling the message: " + e.getMessage());
				e.printStackTrace(); // TBD Log this
				return;
			}
		} else {
			logger.severe("Got Message of Unknown type " + message.getClass().getName());
			return;
		}

		requestContent = request.getMessageContent();
		if (requestContent == null) {
			logger.severe("Request Content is empty");
			return;
		}

		processMessageContent(requestContent, logger);
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	public void processMessageContent(MessageContent requestContent, Logger logger) {
		try {
			// retrieve Transformer Station:
			String tsNumerator = requestContent.getSTATIONNUMERATOR();
			if (tsNumerator == null) {
				logger.severe("Station Numerator is empty");
				return;
			}
			TransformerStation ts = getTransformerStation(tsNumerator);
			if (ts == null) {
				logger.severe("Stransformer Station not found");
				return;
			}

			// Retrieve Transformer:
			String transformerName = requestContent.getNUMTRANSINSCHEMA();
			String transformerExternalName = ts.getExternalName().substring(ts.getExternalName().indexOf("/") + 1) + "/" + transformerName;
			Transformer transformer = getTransformer(transformerExternalName);

			// Set Attributes:
			transformer.setDistrict(ts.getDistrict());
			transformer.setRegion(ts.getRegion());
			transformer.setCity(ts.getCity());
			transformer.setTransformerStation(ts);
			transformer.setHighVoltageLineFeeder(requestContent.getHIGHVOLTAGELINEFEEDER());
			transformer.setCoordinates(getCoordinates(requestContent));
			transformer.setManufacturer(requestContent.getTRANSMANUFACTURER());
			transformer.setSupplyVoltageAmpereRating(new BigDecimal(requestContent.getSUPLAYKVA()));
			transformer.setDryOil(requestContent.getDRYOIL());
			transformer.setTransformerType(""); // TBD
			transformer.setRemarks(""); // TBD
			transformer.setLatestNisUpdateDate(requestContent.getDATE().toGregorianCalendar().getTime());
			
			transformer.setName(transformerName.replaceAll("/", " "));
			transformer.setExternalName("TR/" + transformerExternalName);
			transformer.setActiveDate(new Date(0));
			transformer.setFrom(new Date(0));
			transformer.setParent(ts.getFolder()); // Transformer is places under TS

			transformer.saveChanges();
			logger.info("Transformer Saved");
		} catch (Exception e) {
			logger.severe("Failed preparing the message (General Error): " + e.getMessage());
			e.printStackTrace(); // TBD Log this
			return;
		}
		logger.info("Message Processing Completed");
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	public TransformerStation getTransformerStation(String transformerStationNumerator) {
		TransformerStationFactory tsFactory = IecWarehouse.getInstance().getTransformerStationFactory(); 
		TransformerStation ts = tsFactory.findByKey(transformerStationNumerator);
		return ts;
	}
	
	public Transformer getTransformer(String transformerExternalName) {
		TransformerFactory transformerFactory = IecWarehouse.getInstance().getTransformerFactory(); 
		Transformer transformer = transformerFactory.findByKey(transformerExternalName);
		if (transformer == null)
			transformer = transformerFactory.createNew();
		return transformer;
	}
	
	public SpatialCoordinates getCoordinates(MessageContent requestContent) {
		SpatialCoordinates sc = new SpatialCoordinates();
		sc.setLatitude(new DegreesWorldCoordinate(requestContent.getTRANSKORDINATEX()));
		sc.setLongitude(new DegreesWorldCoordinate(requestContent.getTRANSKORDINATEY()));
		return sc;
	}
}
