package com.ericsson.iec.service;

import java.util.ArrayList;
import java.util.List;

import com.energyict.mdc.channels.ip.socket.OutboundTcpIpConnectionType;
import com.energyict.mdc.tasks.OutboundConnectionTask;

public class OutboundConnectionTaskFilter {

	public List<OutboundConnectionTask> filterOutboundTcpIpConnections(List<OutboundConnectionTask> input) {
		List<OutboundConnectionTask> tasks = new ArrayList<OutboundConnectionTask>();
		for (OutboundConnectionTask task :  input) {
			if (task.getConnectionType() instanceof OutboundTcpIpConnectionType) {
				tasks.add(task);
			}
		}
		return tasks;
	}
}
