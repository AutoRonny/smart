package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface PremiseFactory extends FolderVersionWrapperFactory<Premise> {

	Premise findByKey(String key);
}
