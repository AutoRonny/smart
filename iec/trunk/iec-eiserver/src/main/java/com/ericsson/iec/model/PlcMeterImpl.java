package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;

public class PlcMeterImpl extends GenericMeterImpl implements PlcMeter {

	private static final long serialVersionUID = -1871659369835667823L;

	protected PlcMeterImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

}

