package com.ericsson.iec.model;

import com.ericsson.iec.constants.Warehouses;
import com.ericsson.iec.core.exception.FatalConfigurationException;

public interface WarehouseFactory {

	Warehouse find(Warehouses warehouse) throws FatalConfigurationException;
}
