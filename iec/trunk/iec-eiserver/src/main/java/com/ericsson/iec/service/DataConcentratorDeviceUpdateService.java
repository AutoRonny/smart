package com.ericsson.iec.service;

import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.model.DataConcentratorDevice;

public interface DataConcentratorDeviceUpdateService {

	void updateIpAddress(DataConcentratorDevice device, String ipAddress) throws IecException;

}
