package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;

public class CtMeterGridFactoryImpl extends FolderVersionWrapperFactoryImpl<CtMeterGrid> implements CtMeterGridFactory {

	public CtMeterGridFactoryImpl() {
		super();
	}
	
	public CtMeterGridFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	@Override
	public CtMeterGrid createNew(FolderVersionAdapter arg0) {
		return new CtMeterGridImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return FolderTypes.CT_METER_GRID.name;
	}
	
	@Override
	public CtMeterGrid findByKey(String key) {
		return findByExternalName(FolderTypes.CT_METER_GRID.buildExternalName(key), new Date());
	}
}
