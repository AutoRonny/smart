package com.ericsson.iec.model;

import com.energyict.mdw.core.Folder;
import com.ericsson.iec.constants.Warehouses;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.FatalConfigurationException;

import static com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage.WAREHOUSE_FOLDER_NOT_CONFIGURED;

public class WarehouseFactoryImpl implements WarehouseFactory {

	public Warehouse find(Warehouses warehouse) throws FatalConfigurationException {
		Folder folder = IecWarehouse.getInstance().getMeteringWarehouse().getFolderFactory().findByExternalName(warehouse.getExternalName());
		if (folder == null) {
			throw new FatalConfigurationException(WAREHOUSE_FOLDER_NOT_CONFIGURED, warehouse.getName(), warehouse.getExternalName());
		}
		return new WarehouseImpl(folder);
	}
	
}
