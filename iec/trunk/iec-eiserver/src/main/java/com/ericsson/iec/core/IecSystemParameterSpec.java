package com.ericsson.iec.core;

import java.util.List;

import com.energyict.cuo.core.ApplicationModule;
import com.energyict.mdus.core.SapSystemParameterSpec;
import com.energyict.mdw.core.DefaultSystemParameterSpec;

public class IecSystemParameterSpec extends DefaultSystemParameterSpec {

	public IecSystemParameterSpec(String key, String displayname, String description, String category, String value, List<String> possibleValues) {
		super(key, displayname, description, value, possibleValues);
		setCategory(category);
	}
	
	public IecSystemParameterSpec(SapSystemParameterSpec systemParameter) {
		this(	systemParameter.getKey()
				, systemParameter.getDisplayName()
				, systemParameter.getDescription()
				, systemParameter.getCategory()
				, systemParameter.getDefaultValue()
				, systemParameter.getPossibleValues());
	}
	
	@Override
	public ApplicationModule getModule() {
		return ApplicationModule.CUSTOM;
	}

	
}
