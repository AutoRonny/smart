package com.ericsson.iec.prototype;

import com.ericsson.iec.constants.ProtoTypeConstants.ProtoTypeObjectType;
import com.ericsson.iec.constants.ProtoTypeConstants.ProtoTypes;
import com.ericsson.iec.core.exception.FatalConfigurationException;

public class MeterProtoTypeFactoryImpl extends AbstractProtoTypeFactoryImpl implements MeterProtoTypeFactory {

	private String meterModel;
	
	public MeterProtoTypeFactoryImpl(String meterModel) {
		super();
		this.meterModel = meterModel;
	}
	
	@Override
	public MeterProtoType find() throws FatalConfigurationException {
		return new MeterProtoTypeImpl(getCopySpec());
	}

	@Override
	public String getCopySpecName() {
		ProtoTypes protoTypes = ProtoTypes.getByNamePrefix(meterModel);
		return (protoTypes != null) ? protoTypes.getName() : (meterModel + "..."); // METER_512, METER_061, ... (return moterModel for exception text)
	}

	@Override
	public ProtoTypeObjectType getObjectType() {
		ProtoTypes protoTypes = ProtoTypes.getByNamePrefix(meterModel);
		return (protoTypes != null) ? protoTypes.getObjectType() : null;
	}

	@Override
	public String getMeterModel() {
		return meterModel;
	}
}
