package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface CityFactory extends FolderVersionWrapperFactory<City> {

	City findByKey(String key);
	
}
