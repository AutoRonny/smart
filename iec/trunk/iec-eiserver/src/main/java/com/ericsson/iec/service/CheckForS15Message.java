package com.ericsson.iec.service;

import java.util.logging.Logger;

import com.energyict.mdc.messages.DeviceMessage;
import com.energyict.mdc.messages.DeviceMessageFactoryImpl;
import com.energyict.mdc.messages.DeviceMessageStatus;
import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.DeviceEventFilter;
import com.energyict.mdw.core.DeviceMessageFilter;
import com.ericsson.iec.model.DataConcentratorDeployment;

public class CheckForS15Message {
	
	public static final String TRANSITION_NAME = "Check For S15";
	
	private static final String REBOOT_BY_AC_POWER_MESSAGE = "Reboot by AC Power";
	
	private Logger logger;
	
	public CheckForS15Message(Logger logger) {
		this.logger = logger;
	}

	public final boolean process(DataConcentratorDeployment dataConcentratorDeployment) {
		DeviceEventFilter filter = new DeviceEventFilter();
		filter.setMessageMask(REBOOT_BY_AC_POWER_MESSAGE);
		Device device = dataConcentratorDeployment.getDataConcentrator().getDevice().getDevice();
		return !device.getEvents(filter).isEmpty();
	}

}
