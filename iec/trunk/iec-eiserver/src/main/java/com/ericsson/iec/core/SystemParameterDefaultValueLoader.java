package com.ericsson.iec.core;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.UNABLE_TO_CREATE_SYSTEM_PARAMTER;

import java.sql.SQLException;

import com.energyict.cbo.BusinessException;
import com.energyict.comserver.tools.Strings;
import com.energyict.cuo.core.SystemParameterSpec;
import com.energyict.mdw.core.SystemParameter;
import com.energyict.mdw.shadow.SystemParameterShadow;
import com.energyict.projects.AbstractCustomerModule;
import com.energyict.projects.LibraryModule;
import com.ericsson.iec.core.exception.FatalExecutionException;

public class SystemParameterDefaultValueLoader {

	public SystemParameterDefaultValueLoader() {
	}
	
	public void loadDefaultValues(AbstractCustomerModule customerModule) throws FatalExecutionException {
		for (LibraryModule libraryModule : customerModule.getLibraryModules()) {
			loadDefaultValues(libraryModule);
		}
	}
	
	public void loadDefaultValues(LibraryModule libraryModule) throws FatalExecutionException {
		for (SystemParameterSpec spec : libraryModule.getSystemParameterSpecs()) {
			SystemParameter parameter = IecWarehouse.getInstance().getMeteringWarehouse().getSystemParameterFactory().find(spec.getName());
			if (parameter==null) {
				if (!Strings.isEmpty(spec.getDefaultValue())) {
					createSystemParameter(spec);	
				}
			}
		}
	}
	
	private void createSystemParameter(SystemParameterSpec spec) throws FatalExecutionException {
		SystemParameterShadow shadow = new SystemParameterShadow();
		shadow.setName(spec.getName());
		shadow.setDisplayname(spec.getDisplayname());
		shadow.setDefaultValue(spec.getDefaultValue());
		shadow.setValue(spec.getDefaultValue());
		try {
			IecWarehouse.getInstance().getMeteringWarehouse().getSystemParameterFactory().create(shadow);
		}
		catch (BusinessException | SQLException ex) {
			throw new FatalExecutionException(ex, UNABLE_TO_CREATE_SYSTEM_PARAMTER, spec.getName(), spec.getDisplayname());
		}
	}
	
}
