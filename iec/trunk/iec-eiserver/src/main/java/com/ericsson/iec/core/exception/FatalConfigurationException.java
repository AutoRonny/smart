package com.ericsson.iec.core.exception;

import com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage;

public class FatalConfigurationException extends AbstractConfigurationException {

	private static final long serialVersionUID = -4161286101151051217L;

	public FatalConfigurationException(ConfigurationExceptionMessage exceptionCode, Object... messageParameters) {
		super(exceptionCode, true, messageParameters);
	}

	public FatalConfigurationException(Throwable cause, ConfigurationExceptionMessage exceptionCode, Object... messageParameters) {
		super(cause, exceptionCode, true, messageParameters);
	}

}
