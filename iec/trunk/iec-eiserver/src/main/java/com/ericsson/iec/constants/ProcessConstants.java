package com.ericsson.iec.constants;

public class ProcessConstants {
	
	public enum Processes {
		DATA_CONCENTRATOR_DEPLOYMENT("DataConcentratorDeployment"),
		PLC_METER_REGISTRATION("PlcMeterRegistration"),
		CELLULAR_METER_REGISTRATION("CellularMeterRegistration"),
		METER_REGISTRATION_NOC_NOTIFICATION("MeterRegistrationNocNotification"),
		;
		
		public String name;
		
		Processes(String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
		
	}
}