package com.ericsson.iec.model;

import com.energyict.projects.common.model.processcase.exceptionhandling.ProcessCaseWrapperWithExceptionHandling;

public interface DataConcentratorDeployment extends ProcessCaseWrapperWithExceptionHandling {

	void setRetry(Boolean retry);

	Boolean getRetry();

	void setDeploymentStatus(String deploymentStatus);

	String getDeploymentStatus();

	void setDataConcentrator(DataConcentrator dataConcentrator);

	DataConcentrator getDataConcentrator();
	
	String getTrackingId();
	
	void setTrackingId(String trackingId);
	
}
