package com.ericsson.iec.model;

import static com.ericsson.iec.constants.AttributeTypeConstants.PlcGridAttributes.*;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;

public class PlcGridImpl extends FolderVersionWrapperImpl implements PlcGrid {

	private static final long serialVersionUID = -6571113779229681175L;

	protected PlcGridImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public String getIdentifier() {
		return getStringAttribute(IDENTIFIER.name);
	}

	@Override
	public void setIdentifier(String identifier) {
		setStringAttribute(IDENTIFIER.name, identifier);
	}
}
