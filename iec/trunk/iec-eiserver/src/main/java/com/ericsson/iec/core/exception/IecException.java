package com.ericsson.iec.core.exception;

import com.energyict.mdus.core.BusinessErrorCode;
import com.energyict.mdus.core.BusinessErrorCodeOwner;
import com.energyict.mdus.core.Fatal;
import com.energyict.mdus.core.HasDelay;
import com.energyict.projects.common.exceptions.CustomerException;
import com.energyict.projects.common.exceptions.ExceptionCode;

public abstract class IecException extends CustomerException implements BusinessErrorCodeOwner, Fatal, HasDelay {

	private static final long serialVersionUID = 1131459946867531710L;

	private ExceptionCode exceptionCode;
	private Object[] messageParameters;
	private BusinessErrorCode businessErrorCode;
	private Boolean isFatal;
	private Integer delay;
	
	public IecException(ExceptionCode exceptionCode, Object... messageParameters) {
		super(exceptionCode, messageParameters);
		this.exceptionCode = exceptionCode;
		this.messageParameters = messageParameters;
	}
	
	public IecException(Throwable cause, ExceptionCode code, Object... messageParameters) {
		super(cause, code, messageParameters);
		this.exceptionCode = code;
		this.messageParameters = messageParameters;
	}
	
    @Override
    public String getErrorCode() {
        return getMessageId();
    }

    public ExceptionCode getExceptionCode() {
        return exceptionCode;
    }

    public Object[] getMessageParameters() {
        return messageParameters;
    }

    public void setBusinessErrorCode(BusinessErrorCode businessErrorCode) {
        this.businessErrorCode = businessErrorCode;
    }

    public Boolean getFatal() {
        return isFatal;
    }

    public void setFatal(Boolean fatal) {
        isFatal = fatal;
    }

    public boolean isFatal() {
        if (isFatal == null) {
            throw new UnsupportedOperationException(
                    "isFatal() is not implemented by " + getClass().getName());
        }
        return isFatal;
    }

    public BusinessErrorCode getBusinessErrorCode() {
        if (businessErrorCode == null) {
            throw new UnsupportedOperationException(
                    "getBusinessErrorCode() is not implemented by "
                            + getClass().getName());
        }
        return businessErrorCode;
    }

    /***
     * The delay can be NULL, 0 or greater than 0
     * <p>
     * Null: The default delay is used in SAP MDUS.
     * >= 0: 0 is no delay, it will be processed immediately. Unit is seconds
     *
     * @return delay
     */
    public Integer getDelay() {
        return delay;
    }

    public void setDelay(Integer delay) {
        this.delay = delay;
    }

}
