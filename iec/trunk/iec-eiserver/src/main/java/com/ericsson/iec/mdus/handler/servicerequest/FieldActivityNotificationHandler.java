package com.ericsson.iec.mdus.handler.servicerequest;

import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.energyict.cbo.BusinessException;
import com.energyict.cbo.TimePeriod;
import com.energyict.mdus.core.SapRequestMessage;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.exception.MdusInstantiationException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;
import com.energyict.mdus.core.requesthandler.RequestHandlerParameters;
import com.energyict.mdw.shadow.imp.ConsumptionRequestShadow;
import com.ericsson.iec.core.IecWarehouse;

import nismdm01.interfaces.mdm.iec.TransformerStationRequest;
import wfmmdm01.interfaces.mdm.iec.FieldActivityNotificationRequest;

public class FieldActivityNotificationHandler extends IecSingleRequestHandler<FieldActivityNotificationRequest>{

	public FieldActivityNotificationHandler() throws MarshallingException {
		super();
	}

	@Override
	public void handle(SapRequestMessage sapRequestMessage) throws BusinessException {
		FieldActivityNotificationRequest request = unmarshall(sapRequestMessage.getMessage(), FieldActivityNotificationRequest.class);
		String uuid = request.getMessageHeader().getMessageID();
		RequestHandlerParameters<FieldActivityNotificationRequest> parameters = 
				new RequestHandlerParameters<FieldActivityNotificationRequest>(sapRequestMessage, request, uuid);
		handle(parameters);
	}
	
	protected ConsumptionRequestShadow getConsumptionRequestShadow(RequestHandlerParameters<FieldActivityNotificationRequest> parameters) throws BusinessException {
		FieldActivityNotificationRequest singleRequest = parameters.getRequest();
		try {
			return buildConsumptionRequestShadow(singleRequest);
		} catch (JAXBException e) {
			throw new BusinessException(e);
		}
		
	}

	private ConsumptionRequestShadow buildConsumptionRequestShadow(FieldActivityNotificationRequest singleRequest) throws SystemObjectNotDefined, MdusInstantiationException, SystemParameterIncorrectValue, JAXBException {
		ConsumptionRequestShadow shadow = new ConsumptionRequestShadow();
		TimePeriod validityPeriod = IecWarehouse.getInstance().getIecClassInjector().getIecConsumptionRequestValidityPeriodCalculator().calculateForFieldActivityNotificationRequest(singleRequest);
		
		shadow.setName(singleRequest.getRequest().getSerialNumber());
		shadow.setExternalName(singleRequest.getRequest().getSerialNumber());
		shadow.setFrom(validityPeriod.getFrom());
		shadow.setTo(validityPeriod.getTo());
		
		JAXBContext jaxbContext = JAXBContext.newInstance(FieldActivityNotificationRequest.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		StringWriter writer = new StringWriter();
		marshaller.marshal(singleRequest, writer);
		shadow.setRequest(writer.toString()); 
				
		return shadow;
	}

}
