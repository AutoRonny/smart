package com.ericsson.iec.exporter.meterkey;

import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.Group;
import com.energyict.mdw.core.MeteringWarehouse;

import java.util.*;
import java.util.logging.Logger;

public class MeterKeyExporter {
    private final MeterKeyProcessor processor;
    private final MeterKeyB31Writer writer;
    private final Logger logger;

    public MeterKeyExporter(Logger logger) {
        processor = new MeterKeyProcessor();
        writer = new MeterKeyB31Writer();
        this.logger = logger;
    }

    private HashMap<String, List<Device>> groupDevicesByConcentrator() {
        HashMap<String, List<Device>> devicesByConcentrator = new HashMap<>();
        Group meterKeysGroup = MeteringWarehouse.getCurrent().getGroupFactory().findByName("MeterKeysGroup").get(0);

        for (Object object : meterKeysGroup.getMembers()) {
            Device device = (Device) object;
            String concentratorSerialNumber = device.getGateway() == null ? "CIRCUTOR" : device.getGateway().getSerialNumber();
            if (!devicesByConcentrator.containsKey(concentratorSerialNumber)) {
                List<Device> deviceList = new ArrayList<>();
                deviceList.add(device);
                devicesByConcentrator.put(concentratorSerialNumber, deviceList);
            } else {
                devicesByConcentrator.get(concentratorSerialNumber).add(device);
            }
        }
        return devicesByConcentrator;
    }

    private List<MeterKeyItem> createMeterKeyItems() {
        HashMap<String, List<Device>> devicesByConcentrator = groupDevicesByConcentrator();
        List<MeterKeyItem> meterKeyItems = new ArrayList<>();
        for (Map.Entry<String, List<Device>> entry : devicesByConcentrator.entrySet()) {
            String serialNumberConcentrator = entry.getKey();
            List<Device> deviceList = entry.getValue();
            Collections.sort(deviceList, Comparator.comparing(Device::getSerialNumber));
            MeterKeyItem meterKeyItem = processor.process(serialNumberConcentrator, deviceList);
            meterKeyItems.add(meterKeyItem);
        }
        return meterKeyItems;
    }


    public void export() {
        logger.info("Export started at:" + new Date());
        List<MeterKeyItem> meterKeyItems = createMeterKeyItems();
        if (!meterKeyItems.isEmpty()) {
            for (MeterKeyItem meterKeyItem : meterKeyItems) {
                writer.write(meterKeyItem);
                logger.info("File saved to C:\\MeterKeys");
            }
        }
        logger.info("Export stopped at:" + new Date());
    }
}
