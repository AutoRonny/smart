package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface DataConcentratorFactory extends FolderVersionWrapperFactory<DataConcentrator> {

	DataConcentrator findByKey(String key);
	
}
