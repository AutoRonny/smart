package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;
import com.ericsson.iec.constants.AttributeTypeConstants.DistrictAttributes;

public class DistrictImpl extends FolderVersionWrapperImpl implements District {

	private static final long serialVersionUID = -6571113779229681175L;

	protected DistrictImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public Date getLatestNisUpdateDate() {
		return getDateAndTimeAttribute(DistrictAttributes.LATEST_NIS_UPDATE_DATE.name);
	}
	@Override
	public void setDistrict(Date latestNisUpdateDate) {
		setDateAndTimeAttribute(DistrictAttributes.LATEST_NIS_UPDATE_DATE.name, latestNisUpdateDate);
	}
}
