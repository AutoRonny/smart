package com.ericsson.iec.pluggable.message.handler;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import com.energyict.cbo.BusinessException;
import com.energyict.cpo.PropertySpec;
import com.energyict.cpo.TypedProperties;
import com.energyict.mdw.core.ProtocolEvent;
import com.energyict.mdw.messaging.MessageHandler;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.CellularMeter;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.PlcMeter;
import com.ericsson.iec.model.workflow.CellularMeterRegistration;
import com.ericsson.iec.model.workflow.PlcMeterRegistration;

public class ProtocolEventMessageHandler implements MessageHandler {

	@Override
	public void processMessage(Message message, Logger logger) throws JMSException, BusinessException, SQLException {
		ObjectMessage objectMessage = (ObjectMessage) message;
		if (objectMessage.getObject() instanceof String) {
			String serialId = (String) objectMessage.getObject();
			GenericMeter genericMeter = IecWarehouse.getInstance().getMeterFactory().findBySerialId(serialId);
			if (genericMeter instanceof PlcMeter) {
				PlcMeterRegistration workflow = IecWarehouse.getInstance().getPlcMeterRegistrationFactory().createNew();
				workflow.setMeter(genericMeter);
				workflow.saveChanges();
			}
			if (genericMeter instanceof CellularMeter) {
				CellularMeterRegistration workflow = IecWarehouse.getInstance().getCellularMeterRegistrationFactory().createNew();
				workflow.setMeter(genericMeter);
				workflow.saveChanges();
			}
		}
		
	}
	
	@Override
	public void addProperties(TypedProperties arg0) {
	}

	@Override
	public String getVersion() {
		return "";
	}

	@Override
	public List<PropertySpec> getOptionalProperties() {
		return Collections.emptyList();
	}

	@Override
	public List<PropertySpec> getRequiredProperties() {
		return Collections.emptyList();
	}



}
