package com.ericsson.iec.mdus.handler.servicerequest;

import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.energyict.cbo.BusinessException;
import com.energyict.cbo.TimePeriod;
import com.energyict.mdus.core.SapRequestMessage;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.exception.MdusInstantiationException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;
import com.energyict.mdus.core.requesthandler.RequestHandlerParameters;
import com.energyict.mdw.shadow.imp.ConsumptionRequestShadow;
import com.ericsson.iec.core.IecWarehouse;

import nismdm01.interfaces.mdm.iec.TransformerStationRequest;

public class TransformerStationHandler extends IecSingleRequestHandler<TransformerStationRequest>{

	public TransformerStationHandler() throws MarshallingException {
		super();
	}

	@Override
	public void handle(SapRequestMessage sapRequestMessage) throws BusinessException {
		TransformerStationRequest request = unmarshall(sapRequestMessage.getMessage(), TransformerStationRequest.class);
		String uuid = request.getMessageHeader().getMessageID();
		RequestHandlerParameters<TransformerStationRequest> parameters = 
				new RequestHandlerParameters<TransformerStationRequest>(sapRequestMessage, request, uuid);
		handle(parameters);
	}
	
	protected ConsumptionRequestShadow getConsumptionRequestShadow(RequestHandlerParameters<TransformerStationRequest> parameters) throws BusinessException {
		TransformerStationRequest singleRequest = parameters.getRequest();
		try {
			return buildConsumptionRequestShadow(singleRequest);
		} catch (JAXBException e) {
			throw new BusinessException(e);
		}
		
	}

	private ConsumptionRequestShadow buildConsumptionRequestShadow(TransformerStationRequest singleRequest) throws SystemObjectNotDefined, MdusInstantiationException, SystemParameterIncorrectValue, JAXBException {
		ConsumptionRequestShadow shadow = new ConsumptionRequestShadow();
		TimePeriod validityPeriod = IecWarehouse.getInstance().getIecClassInjector().getIecConsumptionRequestValidityPeriodCalculator().calculateForTransformerStationRequest(singleRequest);
		
		shadow.setName(singleRequest.getMessageContent().getSTATIONNUMERATOR().toString());
		shadow.setExternalName(singleRequest.getMessageContent().getSTATIONNUMERATOR().toString());
		shadow.setFrom(validityPeriod.getFrom());
		shadow.setTo(validityPeriod.getTo());
		
		JAXBContext jaxbContext = JAXBContext.newInstance(TransformerStationRequest.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		StringWriter writer = new StringWriter();
		marshaller.marshal(singleRequest, writer);
		shadow.setRequest(writer.toString()); 
		
				
		return shadow;
	}

}
