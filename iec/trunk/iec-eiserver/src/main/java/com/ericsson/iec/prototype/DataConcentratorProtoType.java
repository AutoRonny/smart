package com.ericsson.iec.prototype;

import java.math.BigDecimal;

import com.ericsson.iec.model.DataConcentrator;

public interface DataConcentratorProtoType extends ProtoType<DataConcentrator> {

	void setDataConcentratorId(String id);
	void setSerialNumber(String serialNumber);
	void setManufacturer(String manufactorer);
	void setManufacturingYear(BigDecimal manufacturingYear);
	void setModelDescription(String modelDescription);
	void setMacAddressPLC(String macAddressPlc);
	void setFirmwareVersion(String firmwareVersion);

}
