package com.ericsson.iec.core.exception;

import com.energyict.mdus.core.BusinessErrorCodeDelegate;
import com.energyict.mdus.core.BusinessErrorSeverity;
import com.energyict.mdus.core.MdusModuleHelper;
import com.energyict.mdus.core.SapProcessingResultCode;
import com.ericsson.iec.core.IecMdusCategoryCodes;
import com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage;

public abstract class AbstractValidationException extends IecException {

	private static final long serialVersionUID = -5566033893339338565L;

	public AbstractValidationException(ValidationExceptionMessage exceptionCode, boolean fatal, Object... messageParameters) {
		super(exceptionCode, messageParameters);
		setFatal(fatal);
		setBusinessErrorCode(new BusinessErrorCodeDelegate(
								SapProcessingResultCode.FAILED, 
								BusinessErrorSeverity.ERROR,
								getMessage(),
								MdusModuleHelper.INSTANCE.get().getDefaultSapTypeId(),
								String.valueOf(IecMdusCategoryCodes.VALIDATION_FAILURE)
				));
	}

	public AbstractValidationException(Throwable cause, ValidationExceptionMessage exceptionCode, boolean fatal, Object... messageParameters) {
		super(cause, exceptionCode, messageParameters);
		setFatal(fatal);
		setBusinessErrorCode(new BusinessErrorCodeDelegate(
								SapProcessingResultCode.FAILED, 
								BusinessErrorSeverity.ERROR,
								getMessage(),
								MdusModuleHelper.INSTANCE.get().getDefaultSapTypeId(),
								String.valueOf(IecMdusCategoryCodes.VALIDATION_FAILURE)
				));
	}
}
