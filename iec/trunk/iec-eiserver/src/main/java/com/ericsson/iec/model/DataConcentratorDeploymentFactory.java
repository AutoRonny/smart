package com.ericsson.iec.model;

import com.energyict.projects.common.model.processcase.ProcessCaseWrapperFactory;

public interface DataConcentratorDeploymentFactory extends ProcessCaseWrapperFactory<DataConcentratorDeployment> {

}
