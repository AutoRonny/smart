package com.ericsson.iec.model;

import static com.ericsson.iec.constants.FolderTypeConstants.FolderTypes.POINT_OF_DELIVERY;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;

public class PointOfDeliveryFactoryImpl extends FolderVersionWrapperFactoryImpl<PointOfDelivery> implements PointOfDeliveryFactory {

	public PointOfDeliveryFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	public PointOfDeliveryFactoryImpl() {
		super();
	}

	@Override
	public PointOfDelivery createNew(FolderVersionAdapter arg0) {
		return new PointOfDeliveryImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return POINT_OF_DELIVERY.name;
	}
	
	@Override
	public PointOfDelivery findByKey(String key) {
		return findByExternalName(POINT_OF_DELIVERY.buildExternalName(key), new Date());
	}
}
