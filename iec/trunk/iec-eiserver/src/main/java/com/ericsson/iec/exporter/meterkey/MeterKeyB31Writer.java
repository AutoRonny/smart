package com.ericsson.iec.exporter.meterkey;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

class MeterKeyB31Writer {

    public MeterKeyB31Writer() {
    }

    public void write(MeterKeyItem meterKeyItem) {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = getRootElement(doc);

            Element cnc = doc.createElement("Cnc");
            rootElement.appendChild(cnc);
            cnc.setAttribute("Id", meterKeyItem.getSerialNumberConcentrator());

            for (MeterKeyModel model : meterKeyItem.getMeterKeyModels()) {
                Element b31 = doc.createElement("B31");
                b31.setAttribute("ActDate", "20180101000000000W");
                b31.setAttribute("CntId", model.getSerialNumberMeter());
                cnc.appendChild(b31);

                Element dasec = doc.createElement("DASec");
                dasec.setAttribute("ClientId", "4");
                dasec.setAttribute("Secret", "00000001");
                b31.appendChild(dasec);

                Element cdtsec = doc.createElement("CDTSec");
                cdtsec.setAttribute("KeyId", "12345678");
                cdtsec.setAttribute("KeyType", "GUnKey");
                cdtsec.setAttribute("KeyVal", model.getEncryptionKey());
                dasec.appendChild(cdtsec);

                Element cdtsec2 = doc.createElement("CDTSec");
                cdtsec2.setAttribute("KeyId", "12345678");
                cdtsec2.setAttribute("KeyType", "GAuKey");
                cdtsec2.setAttribute("KeyVal", model.getAuthenticationKey());
                dasec.appendChild(cdtsec2);
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("C:\\" + meterKeyItem.getSerialNumberConcentrator() + ".xml"));

            transformer.transform(source, result);

        } catch (ParserConfigurationException | TransformerException pce) {
            pce.printStackTrace();
        }
    }

    private Element getRootElement(Document doc) {
        Element rootElement = doc.createElement("Order");
        rootElement.setAttribute("IdReq", "B31");
        rootElement.setAttribute("IdPet", "831");
        rootElement.setAttribute("Version", "3.4");
        doc.appendChild(rootElement);
        return rootElement;
    }
}
