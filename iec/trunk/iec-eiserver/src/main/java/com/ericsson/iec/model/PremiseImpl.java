package com.ericsson.iec.model;

import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.ADDRESS;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.APARTMENT;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.CITY_CODE;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.CITY_NAME;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.COORDINATES;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.DISTRICT;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.ELECTRIC_NET_TYPE;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.ENTRANCE;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.FLOOR;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.HIGH_VOLTAGE_LINE_FEEDER;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.HIGH_VOLTAGE_LINE_FEEDER_NAME;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.LATEST_NIS_UPDATE_DATE;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.LOW_VOLTAGE_LINE_FEEDER_ID;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.METER_USAGE_TYPE;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.PREMISE_TYPE;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.REGION;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.STREET;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.STREET_NUMBER;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.TRANSFORMER;
import static com.ericsson.iec.constants.AttributeTypeConstants.PremiseAttributes.TRANSFORMER_STATION;

import java.util.Date;

import com.energyict.coordinates.SpatialCoordinates;
import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;
import com.ericsson.iec.core.IecWarehouse;

public class PremiseImpl extends FolderVersionWrapperImpl implements Premise {

	private static final long serialVersionUID = -1871659369835667823L;

	protected PremiseImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public TransformerStation getTransformerStation() {
		return getFolderVersionWrapperAttribute(TRANSFORMER_STATION.name, IecWarehouse.getInstance().getTransformerStationFactory());
	}
	@Override
	public void setTransformerStation(TransformerStation transformerStation) {
		setFolderVersionWrapperAttribute(TRANSFORMER_STATION.name, transformerStation);		
	}

	@Override
	public Transformer getTransformer() {
		return getFolderVersionWrapperAttribute(TRANSFORMER.name, IecWarehouse.getInstance().getTransformerFactory());
	}
	@Override
	public void setTransformer(Transformer transformer) {
		setFolderVersionWrapperAttribute(TRANSFORMER.name, transformer);		
	}

	@Override
	public String getHighVoltageLineFeeder() {
		return getStringAttribute(HIGH_VOLTAGE_LINE_FEEDER.name);
	}
	@Override
	public void setHighVoltageLineFeeder(String feeder) {
		setStringAttribute(HIGH_VOLTAGE_LINE_FEEDER.name, feeder);
	}

	@Override
	public String getNameHighVoltageLineFeeder() {
		return getStringAttribute(HIGH_VOLTAGE_LINE_FEEDER_NAME.name);
	}
	@Override
	public void setNameHighVoltageLineFeeder(String feederName) {
		setStringAttribute(HIGH_VOLTAGE_LINE_FEEDER_NAME.name, feederName);
	}

	@Override
	public String getLowVoltageFeederId() {
		return getStringAttribute(LOW_VOLTAGE_LINE_FEEDER_ID.name);
	}
	@Override
	public void setLowVoltageFeederId(String feederId) {
		setStringAttribute(LOW_VOLTAGE_LINE_FEEDER_ID.name, feederId);
	}

	@Override
	public String getPremiseType() {
		return getStringAttribute(PREMISE_TYPE.name);
	}
	@Override
	public void setPremiseType(String premiseType) {
		setStringAttribute(PREMISE_TYPE.name, premiseType);
	}

	@Override
	public String getMeterUsageType() {
		return getStringAttribute(METER_USAGE_TYPE.name);
	}
	@Override
	public void setMeterUsageType(String meterUsageType) {
		setStringAttribute(METER_USAGE_TYPE.name, meterUsageType);
	}

	@Override
	public String getElectricNetType() {
		return getStringAttribute(ELECTRIC_NET_TYPE.name);
	}
	@Override
	public void setElectricNetType(String electricNetType) {
		setStringAttribute(ELECTRIC_NET_TYPE.name, electricNetType);
	}

	@Override
	public String getDistrict() {
		return getStringAttribute(DISTRICT.name);
	}
	@Override
	public void setDistrict(String district) {
		setStringAttribute(DISTRICT.name, district);
	}
	@Override
	public String getRegion() {
		return getStringAttribute(REGION.name);
	}
	@Override
	public void setRegion(String region) {
		setStringAttribute(REGION.name, region);
	}

	@Override
	public String getCityCode() {
		return getStringAttribute(CITY_CODE.name);
	}
	@Override
	public void setCityCode(String cityCode) {
		setStringAttribute(CITY_CODE.name, cityCode);
	}

	@Override
	public String getCityName() {
		return getStringAttribute(CITY_NAME.name);
	}
	@Override
	public void setCityName(String cityName) {
		setStringAttribute(CITY_NAME.name, cityName);
	}

	@Override
	public String getStreet() {
		return getStringAttribute(STREET.name);
	}
	@Override
	public void setStreet(String street) {
		setStringAttribute(STREET.name, street);
	}

	@Override
	public String getStreetNumber() {
		return getStringAttribute(STREET_NUMBER.name);
	}
	@Override
	public void setStreetNumber(String streetNumber) {
		setStringAttribute(STREET_NUMBER.name, streetNumber);
	}

	@Override
	public String getEntrance() {
		return getStringAttribute(ENTRANCE.name);
	}
	@Override
	public void setEntrance(String entrance) {
		setStringAttribute(ENTRANCE.name, entrance);
	}

	@Override
	public String getFloor() {
		return getStringAttribute(FLOOR.name);
	}
	@Override
	public void setFloor(String floor) {
		setStringAttribute(FLOOR.name, floor);
	}

	@Override
	public String getApartment() {
		return getStringAttribute(APARTMENT.name);
	}
	@Override
	public void setApartment(String apartment) {
		setStringAttribute(APARTMENT.name, apartment);
	}

	@Override
	public String getAddress() {
		return getStringAttribute(ADDRESS.name);
	}
	@Override
	public void setAddress(String address) {
		setStringAttribute(ADDRESS.name, address);
	}

	@Override
	public SpatialCoordinates getCoordinates() {
		return getSpatialCoordinatesAttribute(COORDINATES.name);
	}
	
	@Override
	public void setCoordinates(SpatialCoordinates coordinates) {
		setSpatialCoordinatesAttribute(COORDINATES.name, coordinates);
	}

	@Override
	public Date getLatestNisUpdateDate() {
		return getDateAndTimeAttribute(LATEST_NIS_UPDATE_DATE.name);
	}
	@Override
	public void setLatestNisUpdateDate(Date latestNisUpdateDate) {
		setDateAndTimeAttribute(LATEST_NIS_UPDATE_DATE.name, latestNisUpdateDate);
	}
}

