package com.ericsson.iec.core.exception;

import com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage;

public class FatalValidationException extends AbstractValidationException {

	private static final long serialVersionUID = -4161286101151051217L;

	public FatalValidationException(ValidationExceptionMessage exceptionCode, Object... messageParameters) {
		super(exceptionCode, true, messageParameters);
	}

	public FatalValidationException(Throwable cause, ValidationExceptionMessage exceptionCode, Object... messageParameters) {
		super(cause, exceptionCode, true, messageParameters);
	}
}
