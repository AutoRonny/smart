package com.ericsson.iec.mdus.handler.consumptionrequest;

import java.sql.SQLException;
import java.util.logging.Logger;

import com.energyict.cbo.BusinessException;
import com.energyict.cpo.Transaction;
import com.energyict.mdus.core.BusinessErrorCode;
import com.energyict.mdus.core.BusinessErrorCodeProvider;
import com.energyict.mdus.core.Fatal;
import com.energyict.mdus.core.HasDelay;
import com.energyict.mdus.core.MdusModuleHelper;
import com.energyict.mdus.core.SapWebService;
import com.energyict.mdus.core.SapWebServiceCache;
import com.energyict.mdus.core.StackTraceStringBuilder;
import com.energyict.mdus.core.coreaction.ConsumptionRequestErrorAction;
import com.energyict.mdus.core.coreaction.CoreActionExecutor;
import com.energyict.mdus.core.exception.ConsumptionRequestExpiredException;
import com.energyict.mdus.core.exception.NoServiceRequestForConsumptionRequestException;
import com.energyict.mdus.core.model.MdusWarehouse;
import com.energyict.mdus.core.model.relation.SapMdusSubRequest;
import com.energyict.mdw.imp.ConsumptionRequest;
import com.energyict.mdw.importimpl.ConsumptionRequestLoggerCreator;
import com.energyict.mdw.service.ServiceRequest;
import com.energyict.projects.dateandtime.Clock;
import com.ericsson.iec.core.SapMessageQueuesHelper;

/**
 * Transaction used to perform error management when an exception has been caught
 * during the processing of a {@link ConsumptionRequest} in {@link AbstractConsumptionRequestHandler}.
 */
final class ExceptionManagementTransaction implements Transaction<ProcessingStatus>
{
	private final SapMdusSubRequest mdusSubRequest;
	private final int messageDelay;
	private final ConsumptionRequest consumptionRequest;
	private Exception e;

	/**
	 * Constructor.
	 *
	 * @param consumptionRequest for which an error happened during processing.
	 * @param e exception caught.
	 * @param messageDelay default delay used for retry.
	 * @param mdusSubRequest updated by the class. <b>SHOULD NOT BE SAVED IN THIS CLASS!!!</b>
	 */
	protected ExceptionManagementTransaction(ConsumptionRequest consumptionRequest, Exception e, int messageDelay, SapMdusSubRequest mdusSubRequest)
	{
		this.consumptionRequest = consumptionRequest;
		this.e = e;
		this.messageDelay = messageDelay;
		this.mdusSubRequest = mdusSubRequest;
	}

	@Override
	public ProcessingStatus doExecute() throws BusinessException, SQLException
	{
		ProcessingStatus status = ProcessingStatus.FATAL_ERROR;
		boolean requestExpired = hasExpired(consumptionRequest);

		if ( requestExpired && MdusModuleHelper.INSTANCE.get().getUseExpiredBusinessErrorCode() )
		{
			e = new ConsumptionRequestExpiredException(e, consumptionRequest, Clock.INSTANCE.get().now(),
					MdusModuleHelper.INSTANCE.get().getDefaultSapTypeId(),
					MdusModuleHelper.INSTANCE.get().getDefaultSapCategoryCode());
		}

		BusinessErrorCode errorCode = new BusinessErrorCodeProvider().getBusinessErrorCode(e);
		MdusWarehouse.getCurrent().getSapBusinessErrorCodeFactory().createOrUpdateSapBusinessErrorCode(consumptionRequest, errorCode);
		this.mdusSubRequest.setErrorMessage(e.getMessage());
		logStackTraceOnConsumptionRequest(e, consumptionRequest);

		if ( isFatal(e) || requestExpired )
		{
			ConsumptionRequestErrorAction coreAction = new ConsumptionRequestErrorAction(consumptionRequest);
			CoreActionExecutor.getInstance().execute(coreAction);
			status = ProcessingStatus.FATAL_ERROR;
		}
		else
		{
			ServiceRequest serviceRequest = consumptionRequest.getServiceRequest();
			if ( serviceRequest == null )
			{
				throw new NoServiceRequestForConsumptionRequestException(consumptionRequest.getId());
			}
			SapWebService sapWebService = SapWebServiceCache.getInstance().findByServiceRequestType(serviceRequest.getServiceRequestType());
			SapMessageQueuesHelper.sendMessageToConsumptionRequestQueue(
					this.consumptionRequest.getId(),
					sapWebService.getMessagePriority(),
					getDelay(e, this.messageDelay));
			this.mdusSubRequest.setConsumptionRequestQueueDate(Clock.INSTANCE.get().now());
			status = ProcessingStatus.NON_FATAL_ERROR;
		}
		return status;
	}

	protected boolean isFatal(Exception e)
	{
		return (e instanceof Fatal) ? ((Fatal)e).isFatal() : true;
	}

	protected int getDelay(Exception e, int delay)
	{
		if ( (e == null) || (e instanceof HasDelay == false) || (((HasDelay)e).getDelay() == null) )
		{
			return delay;
		}
		return ((HasDelay)e).getDelay();
	}

	protected void logStackTraceOnConsumptionRequest(Exception e, ConsumptionRequest consumptionRequest)
	{
		String stacktrace = StackTraceStringBuilder.getStackTrace(e);
		getLogger(consumptionRequest).severe(stacktrace);
	}

	protected boolean hasExpired(ConsumptionRequest consumptionRequest)
	{
		return (consumptionRequest.getTo() != null &&
				Clock.INSTANCE.get().now().after(consumptionRequest.getTo()));
	}

	protected Logger getLogger(ConsumptionRequest consumptionRequest)
	{
		return ConsumptionRequestLoggerCreator.createLogger(consumptionRequest);
	}
}