package com.ericsson.iec.model.workflow;

import com.energyict.projects.common.model.processcase.ProcessCaseAdapter;
import com.energyict.projects.common.model.processcase.ProcessCaseWrapperFactoryImpl;
import com.ericsson.iec.constants.ProcessConstants;

public class PlcMeterRegistrationFactoryImpl extends ProcessCaseWrapperFactoryImpl<PlcMeterRegistration> 
			 									   implements PlcMeterRegistrationFactory {

	@Override
	public String getProcessName() {
		return ProcessConstants.Processes.PLC_METER_REGISTRATION.name;
	}

	@Override
	public PlcMeterRegistration createNew(ProcessCaseAdapter adapter) {
		return new PlcMeterRegistrationImpl(adapter);
	}

}
