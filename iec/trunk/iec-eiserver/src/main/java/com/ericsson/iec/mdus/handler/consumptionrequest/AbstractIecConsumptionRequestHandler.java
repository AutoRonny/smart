package com.ericsson.iec.mdus.handler.consumptionrequest;

import java.util.logging.Logger;

import com.energyict.mdus.core.consumptionrequesthandler.AbstractConsumptionRequestHandler;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.model.relation.ConsumptionRequestSpecificAttributes;
import com.energyict.mdw.imp.ConsumptionRequest;
import com.ericsson.iec.core.exception.IecException;

public abstract class AbstractIecConsumptionRequestHandler<T> extends AbstractConsumptionRequestHandler<IecException> {
	
	@Override
	public void process(ConsumptionRequest request, ConsumptionRequestSpecificAttributes attributes) throws IecException {
		T message = null;
		try {
			message = unmarshal(request.getRequest());
		} catch (MarshallingException e) {
			e.printStackTrace();
		}
		doProcess(message, getLogger(request), attributes);
	}

	protected abstract void doProcess(T message, Logger logger, ConsumptionRequestSpecificAttributes attributes) throws IecException;
	protected abstract T unmarshal(String request) throws MarshallingException;

}
