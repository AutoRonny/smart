package com.ericsson.iec.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.energyict.hsm.worldline.HsmModelModule;
import com.energyict.hsm.worldline.HsmModule;
import com.energyict.mdus.MdusModule;
import com.energyict.projects.AbstractCustomerModule;
import com.energyict.projects.LibraryModule;
import com.energyict.projects.coreextensions.CoreExtensionsLibraryModule;
import com.energyict.projects.wrappers.WrappersLibraryModule;
import com.ericsson.iec.core.exception.FatalExecutionException;

public class IecCustomerModule extends AbstractCustomerModule {

	public static final String MODULE_NAME = "IEC";
	private static final String CUSTOMER_NAME = "IEC";
	
	@Override
	public String getErrorCodeBundleName() {
		return "";
	}

	@Override
	public String getName() {
		return MODULE_NAME;
	}

	@Override
	public String getAboutPanelCustomer() {
		return CUSTOMER_NAME;
	}

	@Override
	public String getAboutPanelVersion() {
		return "1.0";
	}

	@Override
	public Collection<LibraryModule> getLibraryModules() {
		List<LibraryModule> modules = new ArrayList<>();
		modules.add(new IecLibraryModule());
		modules.add(new MdusModule());
		modules.add(new CoreExtensionsLibraryModule());
		modules.add(new WrappersLibraryModule());
		modules.add(new HsmModelModule());
		return modules;
	}
	
	@Override
	public void initialize() {
		try {
			new SystemParameterDefaultValueLoader().loadDefaultValues(this);
		} catch (FatalExecutionException e) {
			e.printStackTrace();
		}	
	}

}
