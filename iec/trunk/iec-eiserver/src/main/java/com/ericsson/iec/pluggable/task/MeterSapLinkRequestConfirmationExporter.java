package com.ericsson.iec.pluggable.task;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.exporter.ConfirmationExporter;
import com.energyict.mdw.service.ServiceRequestType;
import com.ericsson.iec.mdus.MdusWebservice;

public class MeterSapLinkRequestConfirmationExporter extends ConfirmationExporter {

	@Override
	protected ServiceRequestType getServiceRequestType() throws BusinessException {
		return MdusWebservice.METER_SAP_LINK_REQUEST.getServiceRequestType();
	}

}
