package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface CellularGrid extends FolderVersionWrapper {

	String getIdentifier();

	void setIdentifier(String identifier);

}
