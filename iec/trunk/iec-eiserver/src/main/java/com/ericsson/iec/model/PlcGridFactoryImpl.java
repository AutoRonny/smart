package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;

public class PlcGridFactoryImpl extends FolderVersionWrapperFactoryImpl<PlcGrid> implements PlcGridFactory {

	public PlcGridFactoryImpl() {
		super();
	}
	
	public PlcGridFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	@Override
	public PlcGrid createNew(FolderVersionAdapter arg0) {
		return new PlcGridImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return FolderTypes.PLC_GRID.name;
	}
	
	@Override
	public PlcGrid findByKey(String key) {
		return findByExternalName(FolderTypes.PLC_GRID.buildExternalName(key), new Date());
	}
}
