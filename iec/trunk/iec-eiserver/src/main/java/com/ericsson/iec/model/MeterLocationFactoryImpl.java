package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import static com.ericsson.iec.constants.FolderTypeConstants.FolderTypes.*;

public class MeterLocationFactoryImpl extends FolderVersionWrapperFactoryImpl<MeterLocation> implements MeterLocationFactory {

	public MeterLocationFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	public MeterLocationFactoryImpl() {
		super();
	}

	@Override
	public MeterLocation createNew(FolderVersionAdapter arg0) {
		return new MeterLocationImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return METER_LOCATION.name;
	}
	
	@Override
	public MeterLocation findByKey(String key) {
		return findByExternalName(METER_LOCATION.buildExternalName(key), new Date());
	}
}
