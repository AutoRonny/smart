package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface City extends FolderVersionWrapper {

	District getDistrict();
	void setDistrict(District district);

	Region getRegion();
	void setRegion(Region region);

	Date getLatestNisUpdateDate();
	void setLatestNisUpdateDate(Date latestNisUpdateDate);
}
