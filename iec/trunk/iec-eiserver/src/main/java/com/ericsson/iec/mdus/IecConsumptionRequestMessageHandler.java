package com.ericsson.iec.mdus;

import java.util.List;

import com.energyict.mdus.core.ConsumptionRequestMessageHandler;
import com.energyict.mdus.core.SapWebService;

public class IecConsumptionRequestMessageHandler extends ConsumptionRequestMessageHandler {

	@Override
	protected List<SapWebService> getSapWebServices()
	{
		List<SapWebService> sapWebServices = super.getSapWebServices();
		for (SapWebService sapWebService : MdusWebservice.values() )
		{
			sapWebServices.add(sapWebService);
		}
		return sapWebServices;
	}
}
