package com.ericsson.iec.model;

import java.math.BigDecimal;
import java.util.Date;

import com.energyict.coordinates.SpatialCoordinates;
import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface TransformerStation extends FolderVersionWrapper {

	District getDistrict();
	void setDistrict(District district);

	Region getRegion();
	void setRegion(Region region);
	
	City getCity();
	void setCity(City city);

	String getStationId();
	void setStationId(String stationId);

	String getStationName();
	void setStationName(String stationName);
	
	String getStationLocation();
	void setStationLocation(String stationLocation);

	SpatialCoordinates getCoordinates();
	void setCoordinates(SpatialCoordinates coordinates);

	BigDecimal getStationType();
	void setStationType(BigDecimal stationType);
	
	BigDecimal getNumberOfTransformers();
	void setNumberOfTransformers(BigDecimal numberOfTransformers);
	
	BigDecimal getNumberOfPlcTransformers();
	void setNumberOfPlcTransformers(BigDecimal numberOfPlcTransformers);
	
	String getRemarks();
	void setRemarks(String remarks);

	Date getLatestNisUpdateDate();
	void setLatestNisUpdateDate(Date latestNisUpdateDate);
}
