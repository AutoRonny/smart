package com.ericsson.iec.exporter.meterkey;

class MeterKeyModel {
    private String serialNumberMeter;
    private String authenticationKey;
    private String encryptionKey;

    public String getSerialNumberMeter() {
        return serialNumberMeter;
    }

    public void setSerialNumberMeter(String serialNumberMeter) {
        this.serialNumberMeter = serialNumberMeter;
    }

    public String getAuthenticationKey() {
        return authenticationKey;
    }

    public void setAuthenticationKey(String authenticationKey) {
        this.authenticationKey = authenticationKey;
    }

    public String getEncryptionKey() {
        return encryptionKey;
    }

    public void setEncryptionKey(String encryptionKey) {
        this.encryptionKey = encryptionKey;
    }
}
