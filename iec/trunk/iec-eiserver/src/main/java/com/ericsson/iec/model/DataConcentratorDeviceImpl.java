package com.ericsson.iec.model;

import com.energyict.projects.common.model.device.DeviceAdapter;
import com.energyict.projects.common.model.device.DeviceWrapperImpl;

public class DataConcentratorDeviceImpl extends DeviceWrapperImpl implements DataConcentratorDevice {

	private static final long serialVersionUID = 2647039815641819991L;

	public DataConcentratorDeviceImpl(DeviceAdapter deviceAdapter) {
		super(deviceAdapter);
	}
	
	public void updateIpAddress() {
		
	}

}
