package com.ericsson.iec.prototype;

import static com.ericsson.iec.constants.ProtoTypeConstants.MeterParameters.BATCH;
import static com.ericsson.iec.constants.ProtoTypeConstants.MeterParameters.DELIVERY_DATE;
import static com.ericsson.iec.constants.ProtoTypeConstants.MeterParameters.FIRMWARE_VERSION;
import static com.ericsson.iec.constants.ProtoTypeConstants.MeterParameters.MAC_ADDRESS;
import static com.ericsson.iec.constants.ProtoTypeConstants.MeterParameters.SERIAL_ID;

import java.util.Date;

import com.energyict.mdw.core.Folder;
import com.energyict.mdw.template.CopySpec;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.FirmwareVersion;
import com.ericsson.iec.model.GenericMeter;

public class MeterProtoTypeImpl extends AbstractProtoTypeImpl<GenericMeter> implements MeterProtoType {
	
	protected MeterProtoTypeImpl(CopySpec copySpec) {
		super(copySpec);
	}
	
	@Override
	public GenericMeter wrapFolder(Folder folder) {
		return IecWarehouse.getInstance().getMeterFactory().createNewForLastVersion(folder);
	}
	
	@Override
	public void setSerialId(String serialId) {
		setStringParameter(SERIAL_ID.getName(), serialId);
	}

	@Override
	public void setBatch(String batch) {
		setStringParameter(BATCH.getName(), batch);
	}

	@Override
	public void setMacAddress(String macAddress) {
		setStringParameter(MAC_ADDRESS.getName(), macAddress);
	}


	@Override
	public void setDeliveryDate(Date deliveryDate) {
		setDateParameter(DELIVERY_DATE.getName(), deliveryDate);
	}
	@Override
	public void setFirmwareVersion(FirmwareVersion firmwareVersion) {
		setFolderVersionWrapperAttribute(FIRMWARE_VERSION.getName(), firmwareVersion);
	}
}
