package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;

public class TransformerStationFactoryImpl extends FolderVersionWrapperFactoryImpl<TransformerStation> implements TransformerStationFactory {

	public TransformerStationFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	public TransformerStationFactoryImpl() {
		super();
	}

	@Override
	public TransformerStation createNew(FolderVersionAdapter arg0) {
		return new TransformerStationImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return FolderTypes.TRANSFORMER_STATION.name;
	}
	
	@Override
	public TransformerStation findByKey(String key) {
		return findByExternalName(FolderTypes.TRANSFORMER_STATION.buildExternalName(key), new Date());
	}
}
