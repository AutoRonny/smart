package com.ericsson.iec.core.exception;

import com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage;

public class NonFatalConfigurationException extends AbstractConfigurationException {

	private static final long serialVersionUID = 3651569432558561611L;

	public NonFatalConfigurationException(ConfigurationExceptionMessage exceptionCode, Object... messageParameters) {
		super(exceptionCode, false, messageParameters);
	}

	public NonFatalConfigurationException(Throwable cause, ConfigurationExceptionMessage exceptionCode, Object... messageParameters) {
		super(cause, exceptionCode, false, messageParameters);
	}

}
