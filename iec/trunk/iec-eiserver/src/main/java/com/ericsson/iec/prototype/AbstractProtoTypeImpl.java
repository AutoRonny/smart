package com.ericsson.iec.prototype;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.energyict.cbo.BusinessException;
import com.energyict.cbo.TimePeriod;
import com.energyict.mdw.core.Folder;
import com.energyict.mdw.template.CopySpec;
import com.energyict.projects.common.model.folder.FolderVersionWrapper;
import com.ericsson.iec.model.Warehouse;

public abstract class AbstractProtoTypeImpl<FW extends FolderVersionWrapper> {
	
	private CopySpec copySpec;
	private Date activeDate;
	private Map<String, Object> parameters = new HashMap<String, Object>();
	private Warehouse warehouse;
	
	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}
	
	protected void setStringParameter(String key, String value) {
		parameters.put(key, value);
	}
	
	protected void setBigDecimalParameter(String key, BigDecimal value) {
		parameters.put(key, value);
	}

	protected void setDateParameter(String key, Date value) {
		parameters.put(key, value);
	}

	protected void setFolderVersionWrapperAttribute(String key, FolderVersionWrapper folderVersionWrapper) {
		parameters.put(key, folderVersionWrapper.getFolder());
	}
	
	protected AbstractProtoTypeImpl(CopySpec copySpec) {
		this.copySpec = copySpec;
	}
	
	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public FW save() throws BusinessException, SQLException {
		Folder folder = copySpec.copyTo(warehouse.getFolder(), parameters, activeDate, new TimePeriod(activeDate, null));
		return wrapFolder(folder);
	}
	
	public abstract FW wrapFolder(Folder folder);
	
}
