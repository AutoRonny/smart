package com.ericsson.iec.mdus.handler.servicerequest;

import java.util.Arrays;
import java.util.List;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.requesthandler.AbstractBulkRequestHandler;
import com.energyict.mdus.core.requesthandler.RequestHandlerParameters;
import com.energyict.mdw.shadow.imp.ConsumptionRequestShadow;
import com.ericsson.iec.mdus.IecMarshallerHelper;

public abstract class IecSingleRequestHandler<R> extends AbstractBulkRequestHandler<R, R, MarshallingException> {

	public IecSingleRequestHandler() throws MarshallingException {
		super(IecMarshallerHelper.getInstance());
	}
	
	@Override
	protected List<ConsumptionRequestShadow> getConsumptionRequestShadows(RequestHandlerParameters<R> arg0) throws BusinessException {
		return Arrays.asList(getConsumptionRequestShadow(arg0));	
	}
	
	protected abstract ConsumptionRequestShadow getConsumptionRequestShadow(RequestHandlerParameters<R> arg0) throws BusinessException;

}
