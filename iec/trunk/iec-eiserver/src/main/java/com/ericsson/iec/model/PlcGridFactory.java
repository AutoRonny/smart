package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface PlcGridFactory extends FolderVersionWrapperFactory<PlcGrid> {

	PlcGrid findByKey(String key);
	
}
