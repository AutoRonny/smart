package com.ericsson.iec.model;

import static com.ericsson.iec.constants.AttributeTypeConstants.CtMeterGridAttributes.*;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;

public class CtMeterGridImpl extends FolderVersionWrapperImpl implements CtMeterGrid {

	private static final long serialVersionUID = -6571113779229681175L;

	protected CtMeterGridImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public String getIdentifier() {
		return getStringAttribute(IDENTIFIER.name);
	}

	@Override
	public void setIdentifier(String identifier) {
		setStringAttribute(IDENTIFIER.name, identifier);
	}
}
