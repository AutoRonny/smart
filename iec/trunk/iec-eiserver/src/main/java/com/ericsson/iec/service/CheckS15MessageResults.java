package com.ericsson.iec.service;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;

import java.util.logging.Logger;

import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.DeviceEventFilter;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;

import com.ericsson.iec.model.DataConcentratorDeployment;

public class CheckS15MessageResults extends AbstractServiceTransitionHandlerWithExceptionHandling<DataConcentratorDeployment>{

	public static final String TRANSITION_NAME = "CheckS15MessageResults";
	
	private static final String REBOOT_BY_AC_POWER_MESSAGE = "Reboot by AC Power";
		
	public CheckS15MessageResults(Logger logger) {
		super(logger);
	}

	@Override
	protected void doProcess(DataConcentratorDeployment dataConcentratorDeployment) throws IecException {
		DeviceEventFilter filter = new DeviceEventFilter();
		filter.setMessageMask(REBOOT_BY_AC_POWER_MESSAGE);
		Device device = dataConcentratorDeployment.getDataConcentrator().getDevice().getDevice();
		if (device.getEvents(filter).isEmpty()) {
			dataConcentratorDeployment.setDeploymentStatus("1");
			throw new FatalExecutionException(MESSAGE_PROCESSING_FAILED, "", "");
		}
		dataConcentratorDeployment.setDeploymentStatus("0");
	}
	
	@Override
	protected String getTransitionName() {
		return TRANSITION_NAME;
	}

}
