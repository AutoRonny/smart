package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface CtMeterGridFactory extends FolderVersionWrapperFactory<CtMeterGrid> {

	CtMeterGrid findByKey(String key);
	
}
