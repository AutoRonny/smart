package com.ericsson.iec.mdus.handler.consumptionrequest;

import java.util.Arrays;

import com.ericsson.iec.core.exception.FatalValidationException;

import core.mdm.iec.MessageHeader;
import nismdm01.interfaces.mdm.iec.TransformerStationRequest;
import nismdm01.interfaces.mdm.iec.TransformerStationRequest.MessageContent;

import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.*;

public class TransformerStationValidator {

	private static final String FIELD_NUMBER_OF_TRANSFORMERS_IN_STATION = "NUMTRANSINSTATION";
	
	public void validate(TransformerStationRequest request) throws FatalValidationException {
		validateMessageHeader(request.getMessageHeader());
		validateMessageContent(request.getMessageContent());
	}
	
	private void validateMessageHeader(MessageHeader messageHeader) {
		
	}
	
	private void validateMessageContent(MessageContent messageContent) throws FatalValidationException {
		validateNumberOfTransformers(messageContent.getNUMTRANSINSTATION());
	}
	
	protected void validateNumberOfTransformers(Integer numberOfTransformers) throws FatalValidationException {
		validateFieldIsFilled(FIELD_NUMBER_OF_TRANSFORMERS_IN_STATION, numberOfTransformers);
		validateFieldDoesMatchValues(FIELD_NUMBER_OF_TRANSFORMERS_IN_STATION, numberOfTransformers, new AllowedValues(1, 2, 3, 4));
	}
	
	private void validateFieldIsFilled(String field, Object value) throws FatalValidationException {
		if (value==null) {
			throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, field);
		}
	}
	
	private void validateFieldDoesMatchValues(String field, Object value, AllowedValues allowedValues) throws FatalValidationException {
		if (!allowedValues.contains(value)) {
			throw new FatalValidationException(FIELD_DOES_NOT_MATCH_ANY_VALUE, field, value, allowedValues.toString());
		}
	}
	
}
