package com.ericsson.iec.mdus;

import com.energyict.mdus.core.MarshallerHelper;
import com.energyict.mdus.core.MdusJaxbExceptionConverter;
import com.energyict.mdus.core.exception.MarshallingException;

public class IecMarshallerHelper extends MarshallerHelper<MarshallingException> {

	private static IecMarshallerHelper INSTANCE = null;
	
	protected static final Class<?>[] OBJECT_FACTORIES = new Class<?>[]{
		com.sap.isu.global2.ObjectFactory.class, 
		com.sap.isu.se.global.ObjectFactory.class, 
		nismdm01.interfaces.mdm.iec.ObjectFactory.class,
		nocmdm01.interfaces.mdm.iec.ObjectFactory.class,
		wfmmdm01.interfaces.mdm.iec.ObjectFactory.class,
		sapmdm01.interfaces.mdm.iec.ObjectFactory.class,
		iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.ObjectFactory.class,
		iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.ObjectFactory.class
	};
	
	public IecMarshallerHelper(String namespaceURI) throws MarshallingException {
		super(OBJECT_FACTORIES, new MdusJaxbExceptionConverter());
	}		

	public String marshall(Object msg) throws MarshallingException {
		return marshall(msg, DEFAULT_SAP_NAMESPACE_URI);
	}

	public static IecMarshallerHelper getInstance() throws MarshallingException
	{
		if ( INSTANCE == null ) {
			INSTANCE = new IecMarshallerHelper(DEFAULT_SAP_NAMESPACE_URI);
		}
		return INSTANCE;
	}

	public static void setIntance(IecMarshallerHelper instance){
		INSTANCE = instance;
	}
	
}
