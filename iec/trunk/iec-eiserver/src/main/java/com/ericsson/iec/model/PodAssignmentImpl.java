package com.ericsson.iec.model;

import static com.ericsson.iec.constants.AttributeTypeConstants.PodAssignmentAttributes.METER;
import static com.ericsson.iec.constants.AttributeTypeConstants.PodAssignmentAttributes.POINT_OF_DELIVERY;
import static com.ericsson.iec.constants.AttributeTypeConstants.PodAssignmentAttributes.UNIQUE_IDENTIFIER;

import com.energyict.projects.common.model.relation.RelationAdapter;
import com.energyict.projects.common.model.relation.RelationWrapperImpl;
import com.ericsson.iec.core.IecWarehouse;

public class PodAssignmentImpl extends RelationWrapperImpl implements PodAssignment{

	private static final long serialVersionUID = 7158722396925345126L;

	protected PodAssignmentImpl(RelationAdapter adapter) {
		super(adapter);
	}

	@Override
	public PointOfDelivery getPointOfDelivery() {
		return getFolderVersionWrapperAttribute(POINT_OF_DELIVERY.name, IecWarehouse.getInstance().getPointOfDeliveryFactory());
	}
	@Override
	public void setPointOfDelivery(PointOfDelivery pointOfDelivery) {
		setFolderVersionWrapperAttribute(POINT_OF_DELIVERY.name, pointOfDelivery);
	}

	@Override
	public GenericMeter getMeter() {
		return getFolderVersionWrapperAttribute(METER.name, IecWarehouse.getInstance().getMeterFactory());
	}
	@Override
	public void setMeter(GenericMeter meter) {
		setFolderVersionWrapperAttribute(METER.name, meter);
	}

	@Override
	public String getUniqueIdentifier() {
		return getStringAttribute(UNIQUE_IDENTIFIER.name);
	}
	@Override
	public void setUniqueIdentifier(String uniqueIdentifier) {
		setStringAttribute(UNIQUE_IDENTIFIER.name, uniqueIdentifier);
	}
}
