package com.ericsson.iec.pluggable.imp;

import com.energyict.hsm.worldline.imp.shipment.ShipmentFileImporter;
import com.energyict.hsm.worldline.imp.shipment.ShipmentFileProcessor;

public class IecShipmentFileImporter extends ShipmentFileImporter {

	@Override
	protected ShipmentFileProcessor getProcessor() {
		return new IecShipmentFileProcessor(getLogger());
	}

}
