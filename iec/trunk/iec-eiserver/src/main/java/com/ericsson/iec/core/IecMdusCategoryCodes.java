package com.ericsson.iec.core;

public enum IecMdusCategoryCodes {

	VALIDATION_FAILURE(100),
	EXECUTION_FAILURE(101),
	UNKNOWN_REASON_CODE(201),
	UNKNOWN_PROTOTYPE(301),
	SMART_METER_ALREADY_EXISTS(302),
	OBJECT_DOES_NOT_EXISTS(303);
	
	//TODO Complete list
	
	private int categoryCode;
	
	IecMdusCategoryCodes(int categoryCode) {
		this.categoryCode = categoryCode;
	}
	
	public int getCategoryCode() {
		return categoryCode;
	}
}
