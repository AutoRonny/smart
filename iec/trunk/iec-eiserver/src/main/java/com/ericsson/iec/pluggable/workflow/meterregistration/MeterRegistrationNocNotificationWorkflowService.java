package com.ericsson.iec.pluggable.workflow.meterregistration;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import com.energyict.annotations.processcaseadapter.WorkFlowService;
import com.energyict.bpm.annotations.FlowCondition;
import com.energyict.bpm.annotations.ServiceMethod;
import com.energyict.bpm.annotations.WatchCondition;
import com.energyict.bpm.core.CaseAttributes;
import com.energyict.bpm.core.ProcessCase;
import com.energyict.bpm.core.WorkItem;
import com.energyict.cpo.PropertySpec;
import com.energyict.cpo.PropertySpecFactory;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractWorkflowServiceWithExceptionHandling;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.DataConcentratorDeployment;
import com.ericsson.iec.model.workflow.MeterRegistrationNocNotification;
import com.ericsson.iec.model.workflow.MeterRegistrationNocNotificationFactory;
import com.ericsson.iec.service.BuildAndQueueRegisteredNotificationRequest;
import com.ericsson.iec.service.CheckForB07Message;
import com.ericsson.iec.service.CheckRegisteredNotificationRequest;
import com.ericsson.iec.service.meterregistration.SendNocNotification;

@WorkFlowService(version = "1.0")
public class MeterRegistrationNocNotificationWorkflowService extends AbstractWorkflowServiceWithExceptionHandling<MeterRegistrationNocNotification>  {

	private static final String PROPERTY_USER_FILE_ID = "22";
	
	@ServiceMethod(name=BuildAndQueueRegisteredNotificationRequest.TRANSITION_NAME)
	public CaseAttributes createNocNotificationServiceRequest(WorkItem workItem, Logger logger) {
		return process(workItem, new BuildAndQueueRegisteredNotificationRequest(logger));
	}

	@WatchCondition(name = CheckRegisteredNotificationRequest.TRANSITION_NAME)
	public boolean checkRegisteredNotificationRequest(WorkItem workItem, CaseAttributes caseAttributes, Logger logger) {
		MeterRegistrationNocNotification wrapper = wrap(workItem);
		return new CheckRegisteredNotificationRequest(logger).process(wrapper);
	}

	@FlowCondition(name = "Retry")
	public Boolean retry(ProcessCase processCase, Logger logger) {
		MeterRegistrationNocNotification processCaseWrapper = wrap(processCase);
		return processCaseWrapper.getRetry();
	}
	
	@FlowCondition(name = "No Retry")
	public Boolean noRetry(ProcessCase processCase, Logger logger) {
		return !retry(processCase, logger);
	}
	
	@Override
	public String getVersion() {
		return "Version 1.0";
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<PropertySpec> getOptionalProperties() {
		return Collections.emptyList();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<PropertySpec> getRequiredProperties() {
		return Arrays.asList(PropertySpecFactory.bigDecimalPropertySpec(PROPERTY_USER_FILE_ID));
	}

	@Override
	public MeterRegistrationNocNotificationFactory getProcessCaseWrapperFactory() {
		return IecWarehouse.getInstance().getMeterRegistrationNocNotificationFactory();
	}

}
