package com.ericsson.iec.core.exception;

import com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage;

public class NonFatalExecutionException extends AbstractExecutionException {

	private static final long serialVersionUID = 8329925730977784026L;

	public NonFatalExecutionException(ExecutionExceptionMessage exceptionCode, Object... messageParameters) {
		super(exceptionCode, false, messageParameters);
	}

	public NonFatalExecutionException(Throwable cause, ExecutionExceptionMessage exceptionCode, Object... messageParameters) {
		super(cause, exceptionCode, false, messageParameters);
	}

}
