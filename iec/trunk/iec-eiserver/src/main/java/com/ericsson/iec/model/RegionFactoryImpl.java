package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;

public class RegionFactoryImpl extends FolderVersionWrapperFactoryImpl<Region> implements RegionFactory {

	public RegionFactoryImpl() {
		super();
	}

	public RegionFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	@Override
	public Region createNew(FolderVersionAdapter arg0) {
		return new RegionImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return FolderTypes.REGION.name;
	}

	@Override
	public Region findByKey(String key) {
		return findByExternalName(FolderTypes.REGION.buildExternalName(key), new Date());
	}
}
