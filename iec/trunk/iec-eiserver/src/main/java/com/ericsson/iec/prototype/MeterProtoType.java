package com.ericsson.iec.prototype;

import java.util.Date;

import com.ericsson.iec.model.FirmwareVersion;
import com.ericsson.iec.model.GenericMeter;

public interface MeterProtoType extends ProtoType<GenericMeter> {

	void setSerialId(String serialId);
	void setBatch(String batch);
	void setMacAddress(String macAddress);
	void setDeliveryDate(Date deliveryDate);
	void setFirmwareVersion(FirmwareVersion firmwareVersion);
}
