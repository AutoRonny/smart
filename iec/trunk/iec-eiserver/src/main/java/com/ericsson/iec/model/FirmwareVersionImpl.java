package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;
import com.ericsson.iec.constants.AttributeTypeConstants.FirmwareVersionAttributes;

public class FirmwareVersionImpl extends FolderVersionWrapperImpl implements FirmwareVersion {

	private static final long serialVersionUID = -1871659369835667822L;

	protected FirmwareVersionImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public String getFirmwareVersion() {
		return getStringAttribute(FirmwareVersionAttributes.FIRMWARE_VERSION.name);
	}
	
	@Override
	public void setFirmwareVersion(String firmwareVersion) {
		setStringAttribute(FirmwareVersionAttributes.FIRMWARE_VERSION.name, firmwareVersion);
	}

}
