package com.ericsson.iec.model;

import com.energyict.projects.common.model.processcase.ProcessCaseAdapter;
import com.energyict.projects.common.model.processcase.ProcessCaseWrapperFactoryImpl;
import com.ericsson.iec.constants.ProcessConstants;

public class DataConcentratorDeploymentFactoryImpl extends ProcessCaseWrapperFactoryImpl<DataConcentratorDeployment> 
			 									   implements DataConcentratorDeploymentFactory {

	@Override
	public String getProcessName() {
		return ProcessConstants.Processes.DATA_CONCENTRATOR_DEPLOYMENT.name;
	}

	@Override
	public DataConcentratorDeployment createNew(ProcessCaseAdapter adapter) {
		return new DataConcentratorDeploymentImpl(adapter);
	}

}
