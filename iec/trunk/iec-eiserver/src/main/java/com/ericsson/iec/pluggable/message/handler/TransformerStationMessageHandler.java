package com.ericsson.iec.pluggable.message.handler;

import java.io.StringReader;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.energyict.cbo.BusinessException;
import com.energyict.coordinates.DegreesWorldCoordinate;
import com.energyict.coordinates.SpatialCoordinates;
import com.energyict.cpo.PropertySpec;
import com.energyict.cpo.TypedProperties;
import com.energyict.mdw.messaging.MessageHandler;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.City;
import com.ericsson.iec.model.CityFactory;
import com.ericsson.iec.model.District;
import com.ericsson.iec.model.DistrictFactory;
import com.ericsson.iec.model.Region;
import com.ericsson.iec.model.RegionFactory;
import com.ericsson.iec.model.TransformerStation;
import com.ericsson.iec.model.TransformerStationFactory;

import nismdm01.interfaces.mdm.iec.TransformerStationRequest;
import nismdm01.interfaces.mdm.iec.TransformerStationRequest.MessageContent;
import oracle.jms.AQjmsTextMessage;

@Deprecated
public class TransformerStationMessageHandler implements MessageHandler {

	public TransformerStationMessageHandler() {
	}

	@Override
	public void addProperties(TypedProperties arg0) {
	}

	@Override
	public String getVersion() {
		return "1.0";
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List<PropertySpec> getOptionalProperties() {
		return Collections.emptyList();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List<PropertySpec> getRequiredProperties() {
		return Collections.emptyList();
	}

	@Override
	public void processMessage(Message message, Logger logger) throws JMSException, BusinessException, SQLException {
		TransformerStationRequest request;
		MessageContent requestContent;
		
		if (!(message instanceof oracle.jms.AQjmsTextMessage)) {
			logger.severe("Got Message of Unknown type " + message.getClass().getName());
			return;
		}

		AQjmsTextMessage textMessage = (AQjmsTextMessage)message;
		logger.info("Got Message " + textMessage.getText());
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(TransformerStationRequest.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(textMessage.getText());
			request = (TransformerStationRequest)unmarshaller.unmarshal(reader);
		} catch (JAXBException e) {
			logger.severe("Failed unmarshaling the message: " + e.getMessage());
			e.printStackTrace(); // TBD Log this
			return;
		}

		requestContent = request.getMessageContent();
		if (requestContent == null) {
			logger.severe("Request Content is empty");
			return;
		}
		
		processMessageContent(requestContent, logger);
	}
	
	//-------------------------------------------------------
	
//	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	public void processMessageContent(MessageContent requestContent, Logger logger) {
		String tsNumerator = requestContent.getSTATIONNUMERATOR(); // TBD: check leading zeroes
		if (tsNumerator == null) {
			logger.severe("Station Numerator is empty");
			return;
		}
		
		City city = getCity(requestContent);
		TransformerStation ts = getTransformerStation(tsNumerator);
		
		try {
			ts.setDistrict(getDistrict(requestContent));
			ts.setRegion(getRegion(requestContent));
			ts.setCity(city);
			ts.setStationId(requestContent.getSTATIONID());
			ts.setStationName(requestContent.getSTATIONNAME());
			ts.setStationLocation(requestContent.getSTATIONLOCATION());
			ts.setCoordinates(getCoordinates(requestContent));
			ts.setStationType(new BigDecimal(requestContent.getSTATIONTYPE()));
			ts.setNumberOfTransformers(getNumberOfTransformers(requestContent));
			ts.setNumberOfPlcTransformers(getNumberOfPlcTransformers(requestContent));
			ts.setRemarks(requestContent.getREMARKSFORSTATION());
			ts.setLatestNisUpdateDate(requestContent.getDATE().toGregorianCalendar().getTime());
//			ts.setLatestNisUpdateDate(dateFormat.parse(requestContent.getDATE()));

			ts.setName(tsNumerator.toString());
			ts.setExternalName("TS/" + tsNumerator.toString());
			ts.setActiveDate(new Date(0));
			ts.setFrom(new Date(0));
			ts.setParent(city.getFolder()); // TS is placed  under city

			ts.saveChanges();
			logger.info("Transformer Station Saved");
		} catch (Exception e) {
			logger.severe("Failed preparing the message (General Error): " + e.getMessage());
			e.printStackTrace(); // TBD Log this
			return;
		}
		logger.info("Message Processing Completed");
	}

	//---------------------------------------------------------
	
	private District getDistrict(MessageContent requestContent) {
		DistrictFactory districtFactory = IecWarehouse.getInstance().getDistrictFactory();
		District district = districtFactory.findByKey(String.valueOf(requestContent.getMACHOZKOD()));
		return district;
	}
	
	private Region getRegion(MessageContent requestContent) {
		RegionFactory regionFactory = IecWarehouse.getInstance().getRegionFactory();
		Region region = regionFactory.findByKey(String.valueOf(requestContent.getEZORKOD()));
		return region;
	}

	private City getCity(MessageContent requestContent) {
		CityFactory cityFactory = IecWarehouse.getInstance().getCityFactory();
		String cityCode = requestContent.getCITYKOD();
		City city = null;
		if (cityCode != null)
			city = cityFactory.findByKey(cityCode); // TBD: remove the "0" padding
		return city;
	}
	
	private TransformerStation getTransformerStation(String tsNumerator) {
		TransformerStationFactory tsFactory = IecWarehouse.getInstance().getTransformerStationFactory(); 
		TransformerStation ts = tsFactory.findByKey(tsNumerator);
		if (ts == null)
			ts = tsFactory.createNew();
		return ts;
	}

	private SpatialCoordinates getCoordinates(MessageContent requestContent) {
		SpatialCoordinates sc = new SpatialCoordinates();
		sc.setLatitude(new DegreesWorldCoordinate(requestContent.getSTATIONKORDINATEX()));
		sc.setLongitude(new DegreesWorldCoordinate(requestContent.getSTATIONKORDINATEY()));
		return sc;
	}
	
	private BigDecimal getNumberOfTransformers(MessageContent requestContent) {
		int numberOfTransformersInt = requestContent.getNUMTRANSINSTATION();
		BigDecimal numberOfTransformers = new BigDecimal(numberOfTransformersInt);
		return numberOfTransformers;
	}

	private BigDecimal getNumberOfPlcTransformers(MessageContent requestContent) {
		int numberOfPlcTransformersInt = requestContent.getNUMPLCTRANSINSTATION();
		BigDecimal numberOfPlcTransformers = new BigDecimal(numberOfPlcTransformersInt);
		return numberOfPlcTransformers;
	}
}
