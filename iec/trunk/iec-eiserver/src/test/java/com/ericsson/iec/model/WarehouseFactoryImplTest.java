package com.ericsson.iec.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.energyict.mdw.core.Folder;
import com.energyict.mdw.core.FolderFactory;
import com.energyict.mdw.core.MeteringWarehouse;
import com.ericsson.iec.constants.Warehouses;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.FatalConfigurationException;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)
public class WarehouseFactoryImplTest {
	
	@Rule 
	public ExpectedException exceptions = ExpectedException.none();
	
	private static final String DATA_CONCENTRATOR_WAREHOUSE_NAME = "Dataconcentrator Warehouse";
	private static final String DATA_CONCENTRATOR_WAREHOUSE_EXTERNAL_NAME = "DC_WAREHOUSE";
	
	@Mock MeteringWarehouse meteringWarehouseMock;
	@Mock FolderFactory folderFactoryMock;
	@Mock Folder folderMock;
	
	@Before
	public void init() {
		Mockito.when(meteringWarehouseMock.getFolderFactory()).thenReturn(folderFactoryMock);
		IecWarehouse.getInstance().setMeteringWarehouse(meteringWarehouseMock);
	}
	
	@Test
	public void findDataConcentratorWarehouseWhenConfiguredExpectWarehouseWithCorrectFolder() throws FatalConfigurationException {
		Mockito.when(folderFactoryMock.findByExternalName(DATA_CONCENTRATOR_WAREHOUSE_EXTERNAL_NAME)).thenReturn(folderMock);
		Warehouse warehouse = new WarehouseFactoryImpl().find(Warehouses.DATA_CONCENTRATOR_WAREHOUSE);
		Assert.assertEquals(folderMock, warehouse.getFolder());
	}
	
	@Test
	public void findDataConcentratorWarehouseWhenNotConfiguredExpectWarehouseNotConfiguredException() throws FatalConfigurationException {
		exceptions.expect(FatalConfigurationException.class);
		//exceptions.expectMessage("Warehouse " + DATA_CONCENTRATOR_WAREHOUSE_NAME + " not found based on Externalname "+ DATA_CONCENTRATOR_WAREHOUSE_EXTERNAL_NAME);
		
		Mockito.when(folderFactoryMock.findByExternalName(DATA_CONCENTRATOR_WAREHOUSE_EXTERNAL_NAME)).thenReturn(null);
		new WarehouseFactoryImpl().find(Warehouses.DATA_CONCENTRATOR_WAREHOUSE);
	}
	
	@After
	public void tearDown() {
		IecWarehouse.getInstance().setMeteringWarehouse(null);
	}
	
	
}
