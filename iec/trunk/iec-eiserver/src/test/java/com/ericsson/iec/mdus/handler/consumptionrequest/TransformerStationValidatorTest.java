package com.ericsson.iec.mdus.handler.consumptionrequest;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.MdusModuleHelper;
import com.ericsson.iec.core.exception.FatalValidationException;

@RunWith(MockitoJUnitRunner.class)
public class TransformerStationValidatorTest {
	
	@Rule
	public ExpectedException exceptions = ExpectedException.none();
	
	@Mock
	MdusModuleHelper mdusModuleHelperMock;
	
	@Before
	public void init() throws BusinessException, SQLException {
		MdusModuleHelper.INSTANCE.set(mdusModuleHelperMock);
		Mockito.when(mdusModuleHelperMock.getDefaultSapTypeId()).thenReturn("1");
	}
	
	@Test
	public void validateNumberOfTransformersInStationForNullValueExpectValidationException() throws FatalValidationException {
		exceptions.expect(FatalValidationException.class);
		//exceptions.expectMessage("4545455");
		new TransformerStationValidator().validateNumberOfTransformers(null);
	}
	
	@Test
	public void validateNumberOfTransformersInStationForValidValueOneExpectOk() throws FatalValidationException {
		new TransformerStationValidator().validateNumberOfTransformers(1);
	}
	
	@Test
	public void validateNumberOfTransformersInStationForValidValueTwoExpectOk() throws FatalValidationException {
		new TransformerStationValidator().validateNumberOfTransformers(2);
	}
	
	@Test
	public void validateNumberOfTransformersInStationForValidValueThreeExpectOk() throws FatalValidationException {
		new TransformerStationValidator().validateNumberOfTransformers(3);
	}
	
	@Test
	public void validateNumberOfTransformersInStationForValidValueFourExpectOk() throws FatalValidationException {
		new TransformerStationValidator().validateNumberOfTransformers(4);
	}
	
	@Test
	public void validateNumberOfTransformersInStationForInvalidValueExpectValidationException() throws FatalValidationException {
		exceptions.expect(FatalValidationException.class);
		//exceptions.expectMessage("4545455");
		new TransformerStationValidator().validateNumberOfTransformers(5);
	}
	
}
